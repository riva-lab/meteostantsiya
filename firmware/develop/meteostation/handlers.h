#ifndef HANDLERS_H
#define HANDLERS_H

#include <stdint.h>

/*******************************************************************************/

#define SUMMERTIME_MODE_OFF         0   // летнее время отключено
#define SUMMERTIME_MODE_ON          1   // летнее время включено всегда
#define SUMMERTIME_MODE_AUTO        2   // летнее время устанавливается автоматически

/*******************************************************************************/
/*                         Обработчики для диалога                             */
/*******************************************************************************/

float tempHandler(uint8_t rw_mode, float newValue);
float tempFeelHandler(uint8_t rw_mode, float newValue);
float tempDewHandler(uint8_t rw_mode, float newValue);
float tempUnitHandler(uint8_t rw_mode, float newValue);
float tempValueHandler(uint8_t rw_mode, float newValue);

float humidityHandler(uint8_t rw_mode, float newValue);
float humAbsHandler(uint8_t rw_mode, float newValue);
float steamPresHandler(uint8_t rw_mode, float newValue);
float humValueHandler(uint8_t rw_mode, float newValue);

float pressureHandler(uint8_t rw_mode, float newValue);
float pressUnitHandler(uint8_t rw_mode, float newValue);
float pressAltHandler(uint8_t rw_mode, float newValue);
float pressDeltaHandler(uint8_t rw_mode, float newValue);
float pressDeltaUnitHandler(uint8_t rw_mode, float newValue);
float pressValueHandler(uint8_t rw_mode, float newValue);

float lightHandler(uint8_t rw_mode, float newValue);
float lightCoeffHandler(uint8_t rw_mode, float newValue);

float batteryHandler(uint8_t rw_mode, float newValue);
float battUnitHandler(uint8_t rw_mode, float newValue);
float battLifeHandler(uint8_t rw_mode, float newValue);

float dateYearHandler(uint8_t rw_mode, float newValue);
float dateMonHandler(uint8_t rw_mode, float newValue);
float dateDayHandler(uint8_t rw_mode, float newValue);
float timeHourHandler(uint8_t rw_mode, float newValue);
float timeMinHandler(uint8_t rw_mode, float newValue);
float timeSecHandler(uint8_t rw_mode, float newValue);
float timeSummerHandler(uint8_t rw_mode, float newValue);
float timeSummerModeHandler(uint8_t rw_mode, float newValue);
float timeZoneHourHandler(uint8_t rw_mode, float newValue);
float timeZoneMinHandler(uint8_t rw_mode, float newValue);

float settBrightnessHandler(uint8_t rw_mode, float newValue);
float settInversionHandler(uint8_t rw_mode, float newValue);
float settAutoOffHandler(uint8_t rw_mode, float newValue);
float settDisplayOffHandler(uint8_t rw_mode, float newValue);
float settAltitudeHiHandler(uint8_t rw_mode, float newValue);
float settAltitudeLoHandler(uint8_t rw_mode, float newValue);
float settLogTimeMinHandler(uint8_t rw_mode, float newValue);
float settLogTimeSecHandler(uint8_t rw_mode, float newValue);
float settLogEraseHandler(uint8_t rw_mode, float newValue);
float settUpdateHandler(uint8_t rw_mode, float newValue);

#endif /* HANDLERS_H */
