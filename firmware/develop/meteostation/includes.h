#if defined(ARDUINO) && (ARDUINO >= 100)
#include "Arduino.h"
#else
#include "WProgram.h"
#include <pins_arduino.h>
#endif

#include <stdint.h>

/*******************************************************************************/

// библиотеки
#include <fastIO.h>
#include <Alarms.h>
#include <embedDateTime.h>
#include <SimpleMeteoCalc.h>
#include <SimpleTimeout.h>
#include <SimpleMenu.h>
#include <SimpleSPIFlash.h>
#include <SimpleSSD1306.h>
#include <SimpleBME280.h>
#include <BH1750FVI.h>
#include <avr/pgmspace.h>

/*******************************************************************************/

// файлы проекта
#include "buttons.h"
//#include "connections.h"
//#include "devices.h"
#include "font.h"
//#include "gui.h"
//#include "handlers.h"
//#include "log.h"
//#include "parList.h"
//#include "sett_item.h"
//#include "settings.h"
#include "txt/txt1251.h"

/*******************************************************************************/

#ifdef FAST_IO_IMPLEMENTED

#define _pinMode        fastPinMode
#define _digitalWrite   fastDigitalWrite
#define _digitalRead    fastDigitalRead
#define _shiftOut       fastShiftOut

#else

#define _pinMode        pinMode
#define _digitalWrite   digitalWrite
#define _digitalRead    digitalRead
#define _shiftOut       shiftOut

#endif
