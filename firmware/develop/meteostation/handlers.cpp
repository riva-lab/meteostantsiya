#include "handlers.h"
#include "devices.h"
#include "settings.h"
#include "log.h"
#include "includes.h"
#include "sett_item.h"

/*******************************************************************************/

float tempHandler(uint8_t rw_mode, float newValue) {
  if (setting.temperatureUnit == 1) return mc.getFahrenheits();
  if (setting.temperatureUnit == 2) return mc.getKelvins();
  return mc.getTemperature();
}

float tempFeelHandler(uint8_t rw_mode, float newValue) {
  if (setting.temperatureUnit == 1) return celsiusToFahrenheits(mc.getTemperatureEffective());
  if (setting.temperatureUnit == 2) return celsiusToKelvins(mc.getTemperatureEffective());
  return mc.getTemperatureEffective();
}

float tempDewHandler(uint8_t rw_mode, float newValue) {
  if (setting.temperatureUnit == 1) return celsiusToFahrenheits(mc.getDewPoint());
  if (setting.temperatureUnit == 2) return celsiusToKelvins(mc.getDewPoint());
  return mc.getDewPoint();
}

float tempUnitHandler(uint8_t rw_mode, float newValue) {
  if (rw_mode) setting.temperatureUnit = newValue;
  return setting.temperatureUnit;
}

float tempValueHandler(uint8_t rw_mode, float newValue) {
  if (rw_mode) setting.temperatureValue = newValue;
  return setting.temperatureValue;
}


float humidityHandler(uint8_t rw_mode, float newValue) {
  return mc.getHumidity();
}

float humAbsHandler(uint8_t rw_mode, float newValue) {
  return mc.getHumidityAbsolute();
}

float steamPresHandler(uint8_t rw_mode, float newValue) {
  return mc.getSteamPressurePartial();
}

float humValueHandler(uint8_t rw_mode, float newValue) {
  if (rw_mode) setting.humidityValue = newValue;
  return setting.humidityValue;
}


float pressureHandler(uint8_t rw_mode, float newValue) {
  if (setting.pressureUnit == 1) return mc.getPressuremmHg();
  return mc.getPressure();
}

float pressUnitHandler(uint8_t rw_mode, float newValue) {
  if (rw_mode) setting.pressureUnit = newValue;
  return setting.pressureUnit;
}

float pressAltHandler(uint8_t rw_mode, float newValue) {
  return mc.getAltitude();
}

float pressDeltaHandler(uint8_t rw_mode, float newValue) {
  if (setting.pressureDeltaUnit == 1) return mc.getPressuremmHg() - mc.getNormalPressuremmHg();
  if (setting.pressureDeltaUnit == 2) return mc.getAltitude() - mc.getUserAltitude();
  return mc.getPressure() - mc.getNormalPressure();
}

float pressDeltaUnitHandler(uint8_t rw_mode, float newValue) {
  if (rw_mode) setting.pressureDeltaUnit = newValue;
  return setting.pressureDeltaUnit;
}

float pressValueHandler(uint8_t rw_mode, float newValue) {
  if (rw_mode) setting.pressureValue = newValue;
  return setting.pressureValue;
}


float lightHandler(uint8_t rw_mode, float newValue) {
  return luxValue * 100.0 / (float)setting.lightAttenuation;
}

float lightCoeffHandler(uint8_t rw_mode, float newValue) {
  if (rw_mode) setting.lightAttenuation = 100 - (uint8_t)newValue;
  return 100 - setting.lightAttenuation;
}


float batteryHandler(uint8_t rw_mode, float newValue) {
  if (setting.batteryUnit) return getSystemVoltage();
  return liIonBatteryPercent();
}

float battUnitHandler(uint8_t rw_mode, float newValue) {
  if (rw_mode) setting.batteryUnit = newValue;
  return setting.batteryUnit;
}

float battLifeHandler(uint8_t rw_mode, float newValue) {
  uint8_t b = liIonBatteryPercent();
  if (b < BATTERY_LOW) return 0;
  return (float)(b - BATTERY_LOW) /
         (100.0 / (float)(BATTERY_CAPACITY) * (float)(CONSUMPTION_SLEEP) * 1.05);
}


float dateYearHandler(uint8_t rw_mode, float newValue) {
  uint16_t y;
  uint8_t m;
  uint8_t d;
  utc.updateUTC();
  utc.getDate(&y, &m, &d);
  if (rw_mode) {
    utc.setDate(2000 + (uint8_t)newValue, m, d);
    utc.updateUTC();
  }
  return utc.getYear() - 2000;
}

float dateMonHandler(uint8_t rw_mode, float newValue) {
  uint16_t y;
  uint8_t m;
  uint8_t d;
  utc.updateUTC();
  utc.getDate(&y, &m, &d);
  if (rw_mode) {
    utc.setDate(y, (uint8_t)newValue, d);
    utc.updateUTC();
  }
  return utc.getMonth();
}

float dateDayHandler(uint8_t rw_mode, float newValue) {
  uint16_t y;
  uint8_t m;
  uint8_t d;
  utc.updateUTC();
  utc.getDate(&y, &m, &d);
  if (rw_mode) {
    utc.setDate(y, m, (uint8_t)newValue);
    utc.updateUTC();
  }
  return utc.getDay();
}

float timeHourHandler(uint8_t rw_mode, float newValue) {
  uint8_t h, m, s;
  utc.updateUTC();
  utc.getTime(&h, &m, &s);
  if (rw_mode) {
    utc.setTime((uint8_t)newValue, m, s);
    utc.updateUTC();
  }
  return utc.getHour();
}

float timeMinHandler(uint8_t rw_mode, float newValue) {
  uint8_t h, m, s;
  utc.updateUTC();
  utc.getTime(&h, &m, &s);
  if (rw_mode) {
    utc.setTime(h, (uint8_t)newValue, s);
    utc.updateUTC();
  }
  return utc.getMinute();
}

float timeSecHandler(uint8_t rw_mode, float newValue) {
  uint8_t h, m, s;
  utc.updateUTC();
  utc.getTime(&h, &m, &s);
  if (rw_mode) {
    utc.setTime(h, m, (uint8_t)newValue);
    utc.updateUTC();
  }
  return utc.getSecond();
}

float timeSummerHandler(uint8_t rw_mode, float newValue) {
  if (rw_mode)
    utc.setSummerTimeDelta(setting.timeDSTdelta = (int8_t)newValue);
  return setting.timeDSTdelta;
}

float timeSummerModeHandler(uint8_t rw_mode, float newValue) {
  uint8_t tmp = (uint8_t)newValue;
  if (rw_mode) {
    utc.setSummerTime(tmp == SUMMERTIME_MODE_ON);
    utc.setSummerTimeAuto(tmp == SUMMERTIME_MODE_AUTO);
    setting.timeDSTmode = tmp;
  }
  return setting.timeDSTmode;
}

float timeZoneHourHandler(uint8_t rw_mode, float newValue) {
  if (rw_mode) {
    int16_t tmp = utc.getZone() % 60;
    utc.setZone((int16_t)newValue * 60 + tmp);
    setting.timeZone = utc.getZone();
  }
  return setting.timeZone / 60;
}

float timeZoneMinHandler(uint8_t rw_mode, float newValue) {
  if (rw_mode) {
    int16_t tmp = utc.getZone() / 60;
    utc.setZone((int8_t)newValue + tmp * 60);
    setting.timeZone = utc.getZone();
  }
  return setting.timeZone % 60;
}


float settBrightnessHandler(uint8_t rw_mode, float newValue) {
  if (rw_mode)          setting.displayContrast = (uint8_t)newValue;
  if (setting.displayContrast) disp.contrast(setting.displayContrast - 1);
  else {
    static float x;
    x = 0.05 * luxValue + 0.95 * x;
    if ((uint16_t)x > 1100) disp.contrast(15);
    else                    disp.contrast(log((uint16_t)x) * 2);
  }
  return setting.displayContrast;
}

float settInversionHandler(uint8_t rw_mode, float newValue) {
  if (rw_mode) setting.displayInvert = (uint8_t)newValue;
  if (setting.displayInvert == 0) disp.invert(0);
  if (setting.displayInvert == 1) disp.invert(1);
  if (setting.displayInvert == 2) {
    if ((uint16_t)luxValue > (uint16_t)(AUTOINVERT_LUX_VALUE + 100)) disp.invert(1);
    if ((uint16_t)luxValue < (uint16_t)(AUTOINVERT_LUX_VALUE - 100)) disp.invert(0);
  }
  return setting.displayInvert;
}

float settAutoOffHandler(uint8_t rw_mode, float newValue) {
  if (rw_mode) {
    setting.timePowerOff = (uint8_t)newValue;
    powerOffTimersReset(1);
  }
  return setting.timePowerOff;
}

float settDisplayOffHandler(uint8_t rw_mode, float newValue) {
  if (rw_mode) {
    setting.timeDisplayOff = (uint8_t)newValue;
    powerOffTimersReset(1);
  }
  return setting.timeDisplayOff;
}

float settAltitudeHiHandler(uint8_t rw_mode, float newValue) {
  if (rw_mode) {
    setting.deviceAltitude %= 100;
    setting.deviceAltitude += (uint16_t)100 * (uint8_t)newValue;
  }
  return (float)(setting.deviceAltitude / 100);
}

float settAltitudeLoHandler(uint8_t rw_mode, float newValue) {
  if (rw_mode) {
    setting.deviceAltitude /= 100;
    setting.deviceAltitude *= 100;
    setting.deviceAltitude += (uint8_t)newValue;
  }
  return (float)(setting.deviceAltitude % 100);
}

float settLogTimeMinHandler(uint8_t rw_mode, float newValue) {
  if (rw_mode) setting.logPeriodM = (uint8_t)newValue;
  if (!setting.logPeriodM && !setting.logPeriodS)
    setting.logPeriodS = 5;
  return (float)(setting.logPeriodM);
}

float settLogTimeSecHandler(uint8_t rw_mode, float newValue) {
  if (rw_mode) setting.logPeriodS = (uint8_t)newValue;
  if (!setting.logPeriodM && !setting.logPeriodS)
    setting.logPeriodS = 5;
  return (float)(setting.logPeriodS);
}

float settLogEraseHandler(uint8_t rw_mode, float newValue) {
  if ((uint8_t)(newValue) != 0) {
    disp.textScale(1);
    disp.styleText(TEXT_TM_ADD + TEXT_TC_NOINVERSE + TEXT_TF_AIR + TEXT_TU_NOULINE);
    disp.clear();
    uint8_t y = (SSD1306_HEIGHT - disp.textHeight()) / 2;
    disp.print(getStringPGM(strLogWarning1), JUSTIFY_CENTER, y - disp.textHeight());
    disp.print(getStringPGM(strLogWarning2), JUSTIFY_CENTER, y);
    disp.print(getStringPGM(strLogWarning3), JUSTIFY_CENTER, y + disp.textHeight());
    disp.repaint();
    logErase();
  }
  return 0;
}

float settUpdateHandler(uint8_t rw_mode, float newValue) {
  if (rw_mode)
    setting.updateFlag = (uint8_t)newValue;
  return setting.updateFlag;
}
