#ifndef GUI_H
#define GUI_H

/*******************************************************************************/

#include <stdint.h>
#include "includes.h"

/*******************************************************************************/

#define TIME_SELECTOR_SHOW      3000    // время отображения прямоугольника выбора на домашнем экране
#define TIME_DIALOG_REPEAT_KEYS 100     // время повтора ввода при зажатой кнопке в диалоге
#define TIME_ARROWS_ANIMATION   200     // период кадров анимации стрелок

#define HOME_MENU_ITEMS         7
#define HOME_MENU_ITEM_TEMP     0
#define HOME_MENU_ITEM_BATTERY  1
#define HOME_MENU_ITEM_HUMIDITY 2
#define HOME_MENU_ITEM_DATETIME 3
#define HOME_MENU_ITEM_PRESSURE 4
#define HOME_MENU_ITEM_SETTINGS 5
#define HOME_MENU_ITEM_LIGHT    6

#define Y_OFFSET_TIME           13
#define Y_OFFSET_DATE           36

#define SELECT_WIDTH_BIG        (uint8_t)(0.42 * SSD1306_WIDTH)
#define SELECT_WIDTH_SMALL      (SSD1306_WIDTH - 2 * SELECT_WIDTH_BIG)
#define SELECT_HEIGHT_SMALL     12

/*******************************************************************************/

void screenHome();


#endif /* GUI_H */
