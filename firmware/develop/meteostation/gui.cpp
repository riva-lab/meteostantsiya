#include "gui.h"
#include "SimpleTimeout.h"
#include "SimpleMenu.h"
#include "sett_item.h"
#include "parList.h"
#include "handlers.h"
#include "settings.h"
#include <avr/pgmspace.h>

/*******************************************************************************/

// массив координат прямоугольника-селектора (startX, startY, endX, endY)
const uint8_t selector[] PROGMEM = {
  /* temp / температура */
  0,                                              0,
  SELECT_WIDTH_BIG,                               SELECT_HEIGHT_SMALL,
  /* batt / батарея */
  (SSD1306_WIDTH - SELECT_WIDTH_SMALL) / 2,       0,
  (SSD1306_WIDTH + SELECT_WIDTH_SMALL) / 2 - 1,   SELECT_HEIGHT_SMALL,
  /* hum  / влажность */
  SSD1306_WIDTH - SELECT_WIDTH_BIG,               0,
  SSD1306_WIDTH - 1,                              SELECT_HEIGHT_SMALL,
  /* time / время */
  20,                                             Y_OFFSET_DATE,
  SSD1306_WIDTH - 2 - 20,                         Y_OFFSET_DATE + SELECT_HEIGHT_SMALL,
  /* pres / давление */
  0,                                              SSD1306_HEIGHT - SELECT_HEIGHT_SMALL - 1,
  SELECT_WIDTH_BIG,                               SSD1306_HEIGHT - 1,
  /* sett / настройки */
  (SSD1306_WIDTH - SELECT_WIDTH_SMALL) / 2,       SSD1306_HEIGHT - SELECT_HEIGHT_SMALL - 1,
  (SSD1306_WIDTH + SELECT_WIDTH_SMALL) / 2 - 1,   SSD1306_HEIGHT - 1,
  /* lux  / освещенность */
  SSD1306_WIDTH - SELECT_WIDTH_BIG,               SSD1306_HEIGHT - SELECT_HEIGHT_SMALL - 1,
  SSD1306_WIDTH - 1,                              SSD1306_HEIGHT - 1
};

/*******************************************************************************/

MenuData_t    * menuData;
uint8_t         menuHomeIndex;
uint8_t         selectorVisible;
SimpleTimeout   timeoutSelector(TIME_SELECTOR_SHOW);
SimpleTimeout   timeoutDialog(TIME_DIALOG_REPEAT_KEYS);
SimpleTimeout   timeoutArrows(TIME_ARROWS_ANIMATION);
SimpleTimeout   timeoutConnectPC(500);

/*******************************************************************************/

void drawDisplayPrepare() {
  disp.textScale(1);
  disp.styleBrush(BRUSH_BC_WHITE + BRUSH_BM_ADD + BRUSH_BF_NOFILL);
  disp.styleText(TEXT_TM_ADD + TEXT_TC_NOINVERSE + TEXT_TF_AIR + TEXT_TU_NOULINE);
  disp.clear();
}

void drawArrowsAnimation() {
  static uint8_t arrowOffset = 1;

  if (timeoutArrows.expired()) {
    timeoutArrows.restart();
    --arrowOffset &= 3;
  }

  disp.print(FONT_CHAR_ARROW_L, 2 + arrowOffset, JUSTIFY_DOWN);
  disp.print(FONT_CHAR_ARROW_R, JUSTIFY_RIGHT + 2 + arrowOffset, JUSTIFY_DOWN);
}

void menuHomeDrawSelector(uint8_t item) {
  if (selectorVisible) {
    disp.styleBrush(BRUSH_BC_WHITE + BRUSH_BM_XOR + BRUSH_BF_FILL);
    disp.rectangle(pgm_read_byte(selector + item * 4 + 0),
                   pgm_read_byte(selector + item * 4 + 1),
                   pgm_read_byte(selector + item * 4 + 2),
                   pgm_read_byte(selector + item * 4 + 3) );

    if (timeoutSelector.expired())
      selectorVisible = 0;
  }
}

void drawConnectPCInfo() {
  disp.print(getStringPGM(strHostConect), JUSTIFY_CENTER, JUSTIFY_CENTER);
  disp.repaint();
}

/*******************************************************************************/

void drawHomeTime() {
  disp.setDigits();

  uint8_t h = utc.getHour();
  uint8_t m = utc.getMinute();
  uint8_t x = SSD1306_WIDTH;
  x -= disp.lengthChr(' ');
  x -= disp.lengthChr(':');
  x -= disp.lengthStr(getStringPGMTable(daysOfWeek, utc.getDayOfWeek()));
  x -= disp.lengthNum(h);
  disp.setDigits(2);
  x -= disp.lengthNum(m);
  x -= 5 * 2;
  x /= 2;
  disp.setDigits();

  disp.setCursor(x, Y_OFFSET_TIME);
  disp.print(getStringPGMTable(daysOfWeek, utc.getDayOfWeek()));
  disp.print(' ');
  disp.print(h);
  disp.print(':');
  disp.setDigits(2);
  disp.print(m);
  disp.setDigits();
}

void drawHomeDate() {
  disp.setDigits(2);

  uint16_t y = utc.getYear();
  uint8_t  m = utc.getMonth();
  uint8_t  d = utc.getDay();

  uint8_t x = SSD1306_WIDTH;
  x -= disp.lengthChr('.') * 2;
  x -= disp.lengthNum(y);
  x -= disp.lengthNum(m);
  x -= disp.lengthNum(d);
  x -= 5;
  x /= 2;

  disp.setCursor(x, Y_OFFSET_DATE + 1);
  disp.print(y);
  disp.print('.');
  disp.print(m);
  disp.print('.');
  disp.print(d);
  disp.setDigits();
}

void drawHomeT() {
  switch ((uint8_t)tempValueHandler(0, 0)) {

    case 0:
      disp.print(tempHandler(0, 0), 1, 0, 1);
      break;

    case 1:
      disp.print(FONT_CHAR_MAN, 1, 0);
      disp.print(tempFeelHandler(0, 0), 1);
      break;

    case 2:
      disp.print(FONT_CHAR_HUMIDITY, 1, 0);
      disp.print(tempDewHandler(0, 0), 1);
      break;
  }
  
  disp.print(getStringPGMTable(strTempUnit, (uint8_t)tempUnitHandler(0, 0)));
}

void drawHomeH() {
  float x;
  char *s;
  switch ((uint8_t)humValueHandler(0, 0)) {

    case 0:
      x = humidityHandler(0, 0);
      disp.print(FONT_CHAR_HUMIDITY, JUSTIFY_RIGHT + 3 + disp.lengthNum(x, 0) + disp.lengthChr('%'), 0);
      disp.print(x, 0);
      disp.print('%');
      return;

    case 1:
      x = humAbsHandler(0, 0);
      disp.print(FONT_CHAR_HUMIDITY, JUSTIFY_RIGHT + 2 + disp.lengthNum(x, 1), 0);
      disp.print(x, 1);
      return;

    case 2:
      x = tempDewHandler(0, 0);
      s = getStringPGMTable(strTempUnit, tempUnitHandler(0, 0));
      disp.print(FONT_CHAR_HUMIDITY, JUSTIFY_RIGHT + 3 + disp.lengthNum(x, 1) + disp.lengthStr(s), 0);
      disp.print(x, 1);
      disp.print(s);
      return;

    case 3:
      x = steamPresHandler(0, 0);
      disp.print(FONT_CHAR_HUMIDITY, JUSTIFY_RIGHT + 2 + disp.lengthNum(x, 0), 0);
      disp.print(x, 0);
      return;
  }
}

void drawHomeP() {
  switch ((uint8_t)pressValueHandler(0, 0)) {

    case 0:
      disp.print(FONT_CHAR_PRESSURE, 1, JUSTIFY_DOWN);
      disp.print(pressureHandler(0, 0), pressUnitHandler(0, 0));
      break;

    case 1:
      disp.print(FONT_CHAR_HEIGHT, 1, JUSTIFY_DOWN);
      disp.print(pressAltHandler(0, 0), 1);
      break;

    case 2:
      disp.print(FONT_CHAR_DELTA, 1, JUSTIFY_DOWN);
      disp.print(pressDeltaHandler(0, 0), 1);
      break;
  }
}

void drawHomeL() {
  disp.print(FONT_CHAR_LIGHT, JUSTIFY_RIGHT + 2 + disp.lengthNum(lightHandler(0, 0), 0), JUSTIFY_DOWN);
  disp.print(lightHandler(0, 0), 0);
}

void drawHomeBattery() {
  uint8_t blinkSymbol = (millis() / 1024) & 1;

  if (powerIsCharging() && blinkSymbol)
    disp.print(FONT_CHAR_CHARGING, JUSTIFY_CENTER, 0);
  else if ((liIonBatteryPercent() > BATTERY_LOW) || blinkSymbol) {
    uint8_t battLevel = liIonBatteryPercent() / 12;
    if (battLevel > 8) battLevel = 8;
    disp.print(FONT_CHAR_BATTERY(battLevel), JUSTIFY_CENTER, 0);
  }
}

/*******************************************************************************/

// mode: 0=length, 1=print, 2=print selected
uint8_t printParam(Parameter_t *p, uint8_t mode, float value) {

  if (p->type == PARAM_TYPE_EMPTY) return 0;

  uint8_t length = 0;

  if (mode > 1)
    disp.styleText(TEXT_TM_ADD + TEXT_TC_NOINVERSE + TEXT_TF_AIR + TEXT_TU_ULINE);

  if (p->type == PARAM_TYPE_NUM2)
    disp.setDigits(2);

  if (p->type < PARAM_TYPE_STR) {
    if (mode)   disp.print(value, p->length);
    else        length += disp.lengthNum(value, p->length) + 2;
    value = 0;
    disp.styleText(TEXT_TM_ADD + TEXT_TC_NOINVERSE + TEXT_TF_AIR + TEXT_TU_NOULINE);
  }

  if (p->step == 0) value = 0;
  if (p->type != PARAM_TYPE_EMPTY) {
    char *s = getStringFromParamStruct(p, uint8_t(value));
    if (mode)   disp.print(s);
    else        length += disp.lengthStr(s) + 2;
  }

  disp.setDigits();
  disp.styleText(TEXT_TM_ADD + TEXT_TC_NOINVERSE + TEXT_TF_AIR + TEXT_TU_NOULINE);

  return length;
}

void drawParameters(Parameter_t *p, uint8_t next, uint8_t change) {

  if (p[0].type == PARAM_TYPE_TXT) {
    disp.textScale(1);
    uint8_t y = SSD1306_HEIGHT - disp.textHeight();
    y /= 2;
    y -= disp.textHeight() - 1;

    for (uint8_t i = 0; i < PARAM_DLG_MAX; i++) {
      disp.print(getStringPGM(p[i].str), JUSTIFY_CENTER, y);
      y += disp.textHeight() - 1;
    }

    return;
  }

  static float value[PARAM_DLG_MAX] = {0};

  static uint8_t index = 0;
  if (next) index++;

  disp.textScale(2);
  uint8_t x = SSD1306_WIDTH;
  for (uint8_t i = 0; i < PARAM_DLG_MAX; i++) {

    if (p[i].handler)
      value[i] = p[i].handler(0, 0);

    if (p[i].step) {
      if (i == index) {
        if (change == 0) value[i] -= p[i].step;
        if (change == 2) value[i] += p[i].step;
      }

      if (value[i] > p[i].max) value[i] = p[i].min;
      if (value[i] < p[i].min) value[i] = p[i].max;

      if ((i == index) && (change != 1) && (p[i].handler))
        p[i].handler(1, value[i]);

    } else if (i == index) index++;

    if (index > PARAM_DLG_MAX - 1) index = 0;

    x -= printParam(p + i, 0, value[i]);
  }

  if (x > SSD1306_WIDTH) x = 0; else x /= 2;
  disp.setCursor(x, JUSTIFY_CENTER);
  for (uint8_t i = 0; i < PARAM_DLG_MAX; i++) {
    x = 1;

    if (i == index) {
      if (p[i].step) x = 2;
      else if (++index > PARAM_DLG_MAX - 1) index = 0;
    }

    printParam(p + i, x, value[i]);
  }
  disp.textScale(1);
}

/*******************************************************************************/

uint8_t menuButtonsControl() {
  btn.poll();
  static uint8_t disableButtons;
  disableButtons = isSleepMode();

  if (btn.isActivated()) {
    powerOffTimersReset(1);
    timeoutSelector.restart();
    selectorVisible = 1;

    if (disableButtons) btn.waitForRelease();
    else                return 1;

    timeoutSelector.restart();
  }
  else
    powerOffTimersReset(0);

  return 0;
}

uint8_t menuItemControl(uint8_t item, uint8_t itemTop) {
  static uint8_t next = 0, change = 1;

  utc.update();
  measureWeatherParameters();

  if (isSleepMode())
    waitInSleep();
  else {
    settingItem_t *settItem = getItemStructPGM(menuData->items + item);
    dialog_t      *dlg      = 0;

    drawDisplayPrepare();
    disp.print(getStringPGM(menuData->caption), JUSTIFY_CENTER, 0);
    disp.print(getStringPGM(settItem->caption), JUSTIFY_CENTER, JUSTIFY_DOWN);
    drawArrowsAnimation();

    if (settItem->dialog) {
      dlg = getDlgStructPGM(settItem->dialog);
      drawParameters(dlg->param, next, change);
    }
    next    = 0;
    change  = 1;
    disp.repaint();
  }

  if (menuButtonsControl()) {
    uint8_t bc = btn.clicked();
    next = (bc == BUTTON_OK);
    if (bc == BUTTON_LEFT   ) return MENU_CONTROL_PREV;
    if (bc == BUTTON_RIGHT  ) return MENU_CONTROL_NEXT;

    uint8_t bp = btn.pressed();
    if (bp == BUTTON_OK     ) return MENU_CONTROL_EXIT;

    uint8_t bh = btn.holded();
    if (bh == BUTTON_LEFT   ) return MENU_CONTROL_PREV_TRY;
    if (bh == BUTTON_RIGHT  ) return MENU_CONTROL_NEXT_TRY;

    if (timeoutDialog.expired()) {
      timeoutDialog.restart();
      if (bh != BUTTON_NONE ) bc = bh;
    }

    if (bc == BUTTON_UP     ) change++;
    if (bc == BUTTON_DOWN   ) change--;
  }

  return MENU_CONTROL_NO;
}

uint8_t menuHomeControl(uint8_t item, uint8_t itemTop) {
  utc.update();
  measureWeatherParameters();

  if (isSleepMode())
    waitInSleep();
  else {

    if (timeoutConnectPC.expired()) {
      drawDisplayPrepare();
      drawHomeDate();
      drawHomeT();
      drawHomeH();
      drawHomeP();
      drawHomeL();
      drawHomeBattery();
      disp.print(FONT_CHAR_SETTINGS, JUSTIFY_CENTER, 52);
      disp.textScale(2);
      drawHomeTime();
      menuHomeDrawSelector(item);

      disp.repaint();
    }
  }

  if (connectHostUpdate()) {
    timeoutConnectPC.restart();
    drawDisplayPrepare();
    drawConnectPCInfo();
    return MENU_CONTROL_NO;
  }

  if (menuButtonsControl()) {
    uint8_t bc = btn.clicked();
    if (bc == BUTTON_OK     ) return MENU_CONTROL_ENTER;
    if (bc == BUTTON_LEFT   ) return MENU_CONTROL_PREV;
    if (bc == BUTTON_RIGHT  ) return MENU_CONTROL_NEXT;
    if (bc == BUTTON_UP     ) return MENU_CONTROL_PREV;
    if (bc == BUTTON_DOWN   ) return MENU_CONTROL_NEXT;

    uint8_t bp = btn.pressed();
    if (bp == BUTTON_OK     ) return MENU_CONTROL_EXIT;

    uint8_t bh = btn.holded();
    if (bh == BUTTON_LEFT   ) return MENU_CONTROL_PREV_TRY;
    if (bh == BUTTON_RIGHT  ) return MENU_CONTROL_NEXT_TRY;
    if (bh == BUTTON_UP     ) return MENU_CONTROL_PREV_TRY;
    if (bh == BUTTON_DOWN   ) return MENU_CONTROL_NEXT_TRY;
  }

  return MENU_CONTROL_NO;
}

/*******************************************************************************/

void menuHomeEnter(uint8_t item) {
  menuHomeIndex = item;
  menuData = getMenuDataPGM(menuHome, item);

  SimpleMenu menu;
  menu.begin(0, menuData->count - 1);
  menu.onControl(menuItemControl);
  menu.slidingTime(500);
  menu.exitTime(0);
  menu.show();
}

void screenHome() {
  SimpleMenu menu;
  menu.begin(0, HOME_MENU_ITEMS - 1);
  menu.onEnter(menuHomeEnter);
  menu.onControl(menuHomeControl);
  menu.slidingTime(500);
  menu.exitTime(0);
  menu.show();
}
