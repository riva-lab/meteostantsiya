// Название шрифта        : Verdana 10 Special
// Автор шрифта           : Riva
// Дата и время генерации : 26.04.2021 11:30:55
// Сгенерировано          : Матричный знакогенератор v0.0.0.39
// (c) Tiger ElectroniX Software 
 

#ifndef FONT_VERDANA_10_SPECIAL_H
#define FONT_VERDANA_10_SPECIAL_H


#ifndef FONT_TYPE_MONOSPACED
#define FONT_TYPE_MONOSPACED            0
#endif
#ifndef FONT_TYPE_PROPORTIONAL
#define FONT_TYPE_PROPORTIONAL          1
#endif


#define FONT_VERDANA_10_SPECIAL_ONLY_ASCII      0 // 1=(7 бит ASCII), 0=(8 бит Win1251)
#define FONT_VERDANA_10_SPECIAL_LENGTH          (224 - FONT_VERDANA_10_SPECIAL_ONLY_ASCII * 128)
#define FONT_VERDANA_10_SPECIAL_START_CHAR      32
#define FONT_VERDANA_10_SPECIAL_CHAR_WIDTH      10
#define FONT_VERDANA_10_SPECIAL_CHAR_HEIGHT     12
#define FONT_VERDANA_10_SPECIAL_FONT_TYPE       (FONT_TYPE_PROPORTIONAL)
#define FONT_VERDANA_10_SPECIAL_ARRAY_STEP      (1 + FONT_VERDANA_10_SPECIAL_CHAR_WIDTH * 2)
#define FONT_VERDANA_10_SPECIAL_ARRAY_LENGTH    (FONT_VERDANA_10_SPECIAL_LENGTH * (1 + FONT_VERDANA_10_SPECIAL_CHAR_WIDTH * 2))

extern const unsigned char font_verdana_10_special[];

#include "SSD1306_Fonts.h"
extern const DisplayFont_t fontVerdana10Special;

// спецсимволы
#define FONT_CHAR_CLOCK         ((char)128)
#define FONT_CHAR_SETTINGS      ((char)129)
#define FONT_CHAR_HUMIDITY      ((char)130)
#define FONT_CHAR_LIGHT         ((char)131)
#define FONT_CHAR_PRESSURE      ((char)132)
#define FONT_CHAR_MSQUARE       ((char)134)
#define FONT_CHAR_INVERSE       ((char)135)
#define FONT_CHAR_H2O           ((char)136)
#define FONT_CHAR_GRAPH         ((char)137)
#define FONT_CHAR_HOME          ((char)138)
#define FONT_CHAR_WEEKDAY(x)    ((char)((uint8_t)140 + x))
#define FONT_CHAR_INFO          ((char)147)
#define FONT_CHAR_HELP          ((char)148)
#define FONT_CHAR_OK            ((char)150)
#define FONT_CHAR_CANCEL        ((char)151)
#define FONT_CHAR_DATA          ((char)152)
#define FONT_CHAR_CUBE          ((char)154) // 0x9A
#define FONT_CHAR_BATTERY(x)    ((char)((uint8_t)156 + x))
#define FONT_CHAR_CHARGING      ((char)165)
#define FONT_CHAR_DELTA         ((char)166)
#define FONT_CHAR_HEIGHT        ((char)172)
#define FONT_CHAR_MAN           ((char)173)
#define FONT_CHAR_DEGREE        ((char)176) // 0xB0
#define FONT_CHAR_PLUSMINUS     ((char)177)
#define FONT_CHAR_MICRO         ((char)181)
#define FONT_CHAR_TEMP_FACT     ((char)188)
#define FONT_CHAR_TEMP_ROSA     ((char)189)
#define FONT_CHAR_TEMP_REL      ((char)190)
#define FONT_CHAR_ARROW_L       ((char)139)
#define FONT_CHAR_ARROW_R       ((char)155)

#endif /* FONT_VERDANA_10_H */
