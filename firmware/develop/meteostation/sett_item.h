#ifndef SETT_ITEM_H
#define SETT_ITEM_H

/*******************************************************************************/

#include <stdint.h>
#include <avr/pgmspace.h>

#if defined(ARDUINO) && (ARDUINO >= 100)
#include "Arduino.h"
#else
#include "WProgram.h"
#include <pins_arduino.h>
#endif

/*******************************************************************************/

#define PARAM_DLG_MAX       3   // максимальное количество параметров в диалоге

#define PARAM_TYPE_EMPTY    0   // тип параметра - нет (не выводится)
#define PARAM_TYPE_NUM      1   // тип параметра - число
#define PARAM_TYPE_NUM2     2   // тип параметра - число c мин. 2 знаками в целой части
#define PARAM_TYPE_STR      3   // тип параметра - текст из массива по индексу
#define PARAM_TYPE_TXT      4   // тип параметра - вывести весь текст из массива (неизменяемый)

#define PARAM_DELIM_NO      0   // разделитель параметра - нет
#define PARAM_DELIM_DOT1    '.' // разделитель параметра - 1 точка после
#define PARAM_DELIM_DOT2    ':' // разделитель параметра - 2 точки после

#define PARAM_STR_BUFFER    18  // длина строкового буфера


#define PARAM_EMPTY()       { PARAM_TYPE_EMPTY, 0,        0,0,0,0,0 }
#define PARAM_LABEL(strPtr) { PARAM_TYPE_STR,   (strPtr), 0,0,0,0,0 }
#define PARAM_TEXT(strPtr)  { PARAM_TYPE_TXT,   (strPtr), 0,0,0,0,0 }


/*********************************************************************/
/*               Описание структур для пунктов настроек              */
/*********************************************************************/

// тип: обработчик получения/записи параметра
typedef float (*parameterHandler_t)(uint8_t rw_mode, float newValue);

// структура параметра
typedef struct {
  uint8_t               type;       // тип
  void                * str;        // указатель на начало массива строк
  int8_t                min;        // мин. значение
  int8_t                max;        // макс. значение
  uint8_t               step;       // шаг изменения параметра, 0 = нередактируемый параметр
  uint8_t               length;     // кол-во знаков после запятой
  parameterHandler_t    handler;    // обработчик получения/записи параметра
} Parameter_t;

// диалог - массив параметров
typedef struct {
  Parameter_t       param[PARAM_DLG_MAX];
} dialog_t;

// структура пункта настройки
typedef struct {
  char            * caption;        // указатель на строку с названием пункта
  dialog_t        * dialog;         // указатель на диалог - массив параметров
} settingItem_t;

// структура пункта главного меню
typedef struct {
  uint8_t           count;          // количество пунктов настроек
  settingItem_t   * items;          // указатель на массив со структурами пунктов настроек
  char            * caption;        // указатель на строку с названием главного меню
} MenuData_t;


/*******************************************************************************/
/*     доступ к структурам, хранящимся в Flash (использование PROGMEM)         */
/*******************************************************************************/

dialog_t      * getDlgStructPGM     (const dialog_t       * dialog  PROGMEM);
settingItem_t * getItemStructPGM    (const settingItem_t  * siArr   PROGMEM);
MenuData_t    * getMenuDataPGM      (const MenuData_t     * menu    PROGMEM, uint8_t index);
char          * getStringPGM        (const char           * strArr  PROGMEM);
char          * getStringPGMTable   (void                 * strArr  PROGMEM, uint8_t index);
char          * getStringFromParamStruct(Parameter_t *p, uint8_t index);

#endif /* SETT_ITEM_H */
