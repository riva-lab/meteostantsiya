#ifndef CONNECTIONS_H
#define CONNECTIONS_H


#include "includes.h"

/*******************************************************************************/
/*                         Подключение периферии к МК                          */
/*******************************************************************************/

#define PIN_BUTTON_UP       4   // кнопка ВВЕРХ
#define PIN_BUTTON_DOWN     7   // кнопка ВНИЗ
#define PIN_BUTTON_LEFT     8   // кнопка ВЛЕВО
#define PIN_BUTTON_RIGHT    9   // кнопка ВПРАВО
#define PIN_BUTTON_OK       2   // кнопка ОК


#define PIN_CHARGING_STATE  5   // состояние зарядки
#define PIN_LOCK_POWER      3   // блокировка питания
#define PIN_LOCK_RESET      A0  // блокировка RESET
#define PIN_HOST_ACTIVE     A3  // обнаружение хоста


#define PIN_I2C_SDA         A4  // линия I2C SDA
#define PIN_I2C_SCL         A5  // линия I2C SCL


#define PIN_SPI_MOSI        11  // линия SPI MOSI
#define PIN_SPI_MISO        12  // линия SPI MISO
#define PIN_SPI_SCK         13  // линия SPI SCK
#define PIN_FLASH_CS        10  // линия выбора SPI FLASH


#endif /* CONNECTIONS_H */
