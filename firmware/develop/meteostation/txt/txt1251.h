#ifndef TXT_LABELS_H
#define TXT_LABELS_H

// ��������! ���� ���� ������ ���� � ��������� Win1251.


#include "versions.h"
#define __mkstr(s)  # s
#define mkstr(s)    __mkstr(s)



/*******************************************************************************/
/*******************************************************************************/

// ���� ����������: 0=EN, 1=RU
#define LANGUAGE            1

#if (LANGUAGE == 0)     // English localization
#define TXT_INFO_LANG       "EN"
#endif

#if (LANGUAGE == 1)     // ���� ���������� �������
#define TXT_INFO_LANG       "RU"
#endif



/*******************************************************************************/
/*******************************************************************************/

// ���������� �� ����������
/*
#define TXT_INFO_DATE           "2021.05.01"
#define TXT_INFO_VERSION        "FW v0.0-EN"
#define TXT_INFO_AUTHOR         "(c) RIVA"
#define TXT_INFO_NAME           "Meteostation"
*/

const char strInfo[] PROGMEM =
  FW_VERSION_DATE"\0"           /* TXT_INFO_DATE  L=10 */
  "FW v" mkstr(FW_VERSION_MAJOR) "." mkstr(FW_VERSION_MINOR) "-" TXT_INFO_LANG"\0"   /* TXT_INFO_VERSION  L=10 */
  "(c) RIVA""\0"                /* TXT_INFO_AUTHOR  L=8 */
  "Meteostation""\0"            /* TXT_INFO_NAME  L=12 */;

#define TXT_INFO_DATE           (const char *)(strInfo + 0)
#define TXT_INFO_VERSION        (const char *)(strInfo + 11)
#define TXT_INFO_AUTHOR         (const char *)(strInfo + 22)
#define TXT_INFO_NAME           (const char *)(strInfo + 31)

#define TXT_INFO_DATE_LENGTH    10
#define TXT_INFO_VERSION_LENGTH 10
#define TXT_INFO_AUTHOR_LENGTH  8
#define TXT_INFO_NAME_LENGTH    12



/*******************************************************************************/
/*******************************************************************************/


#if (LANGUAGE == 0)     // English localization

/*******************************************************************************/
/*
#define TXT_DAY_1               "Sn"
#define TXT_DAY_2               "Mn"
#define TXT_DAY_3               "Tu"
#define TXT_DAY_4               "We"
#define TXT_DAY_5               "Th"
#define TXT_DAY_6               "Fr"
#define TXT_DAY_7               "St"
*/
const char strWeekDay[] PROGMEM =
  "Sn""\0"                      /* TXT_DAY_1  L=2 */
  "Mn""\0"                      /* TXT_DAY_2  L=2 */
  "Tu""\0"                      /* TXT_DAY_3  L=2 */
  "We""\0"                      /* TXT_DAY_4  L=2 */
  "Th""\0"                      /* TXT_DAY_5  L=2 */
  "Fr""\0"                      /* TXT_DAY_6  L=2 */
  "St""\0"                      /* TXT_DAY_7  L=2 */;

#define TXT_DAY_1               (const char *)(strWeekDay + 0)
#define TXT_DAY_2               (const char *)(strWeekDay + 3)
#define TXT_DAY_3               (const char *)(strWeekDay + 6)
#define TXT_DAY_4               (const char *)(strWeekDay + 9)
#define TXT_DAY_5               (const char *)(strWeekDay + 12)
#define TXT_DAY_6               (const char *)(strWeekDay + 15)
#define TXT_DAY_7               (const char *)(strWeekDay + 18)

#define TXT_DAY_1_LENGTH        2
#define TXT_DAY_2_LENGTH        2
#define TXT_DAY_3_LENGTH        2
#define TXT_DAY_4_LENGTH        2
#define TXT_DAY_5_LENGTH        2
#define TXT_DAY_6_LENGTH        2
#define TXT_DAY_7_LENGTH        2

/*******************************************************************************/
/*
#define TXT_DELIM_SPACE         " "
#define TXT_DELIM_DOT           "."
#define TXT_DELIM_COLON         ":"
#define TXT_DELIM_LUX           "lx"
#define TXT_DELIM_UTC           "UTC "
#define TXT_DELIM_HOUR          "h "
#define TXT_DELIM_MINUTE        "m"
#define TXT_DELIM_MINSP         "m "
#define TXT_DELIM_METER         "m"
#define TXT_DELIM_PRCNT         "%"
#define TXT_DELIM_GMCUBE        "g/m\x9A"
#define TXT_DELIM_SECOND        "s"
*/

const char strDelimiter[] PROGMEM =
  " ""\0"                       /* TXT_DELIM_SPACE  L=1 */
  ".""\0"                       /* TXT_DELIM_DOT  L=1 */
  ":""\0"                       /* TXT_DELIM_COLON  L=1 */
  "lx""\0"                      /* TXT_DELIM_LUX  L=2 */
  "UTC ""\0"                    /* TXT_DELIM_UTC  L=4 */
  "h ""\0"                      /* TXT_DELIM_HOUR  L=2 */
  "m""\0"                       /* TXT_DELIM_MINUTE  L=1 */
  "m ""\0"                      /* TXT_DELIM_MINSP  L=2 */
  "m""\0"                       /* TXT_DELIM_METER  L=1 */
  "%""\0"                       /* TXT_DELIM_PRCNT  L=1 */
  "g/m\x9A""\0"                 /* TXT_DELIM_GMCUBE  L=4 */
  "s""\0"                       /* TXT_DELIM_SECOND  L=1 */;

#define TXT_DELIM_SPACE         (const char *)(strDelimiter + 0)
#define TXT_DELIM_DOT           (const char *)(strDelimiter + 2)
#define TXT_DELIM_COLON         (const char *)(strDelimiter + 4)
#define TXT_DELIM_LUX           (const char *)(strDelimiter + 6)
#define TXT_DELIM_UTC           (const char *)(strDelimiter + 9)
#define TXT_DELIM_HOUR          (const char *)(strDelimiter + 14)
#define TXT_DELIM_MINUTE        (const char *)(strDelimiter + 17)
#define TXT_DELIM_MINSP         (const char *)(strDelimiter + 19)
#define TXT_DELIM_METER         (const char *)(strDelimiter + 22)
#define TXT_DELIM_PRCNT         (const char *)(strDelimiter + 24)
#define TXT_DELIM_GMCUBE        (const char *)(strDelimiter + 26)
#define TXT_DELIM_SECOND        (const char *)(strDelimiter + 31)
#define TXT_DELIM_NONE          (const char *)(strDelimiter + TXT_DELIM_SPACE_LENGTH)

#define TXT_DELIM_SPACE_LENGTH  1
#define TXT_DELIM_DOT_LENGTH    1
#define TXT_DELIM_COLON_LENGTH  1
#define TXT_DELIM_LUX_LENGTH    2
#define TXT_DELIM_UTC_LENGTH    4
#define TXT_DELIM_HOUR_LENGTH   2
#define TXT_DELIM_MINUTE_LENGTH 1
#define TXT_DELIM_MINSP_LENGTH  2
#define TXT_DELIM_METER_LENGTH  1
#define TXT_DELIM_PRCNT_LENGTH  1
#define TXT_DELIM_GMCUBE_LENGTH 4
#define TXT_DELIM_SECOND_LENGTH 1

/*******************************************************************************/
/*
#define TXT_OFF                 "off"
#define TXT_ON                  "on"
#define TXT_AUTO                "auto"
#define TXT_TCURR               "current"
#define TXT_TFEEL               "feeling"
#define TXT_TDEWP               "dew p-t"
#define TXT_TCELS               "\xB0""C"
#define TXT_TFAHR               "\xB0""F"
#define TXT_TKELV               "K"
#define TXT_PPA                 "Pa"
#define TXT_PMMHG               "mmHg"
#define TXT_PMETER              "m"
#define TXT_PPRES               "pressure"
#define TXT_PALT                "altitude"
#define TXT_PDELTA              "delta"
#define TXT_HREL                "relative"
#define TXT_HABS                "absolute"
#define TXT_HDEWP               "dew p-t"
#define TXT_HSTEAM              "steam p."
#define TXT_BATTV               "mV"
*/

const char strItemMenu[] PROGMEM =
  "off""\0"                     /* TXT_OFF  L=3 */
  "on""\0"                      /* TXT_ON  L=2 */
  "auto""\0"                    /* TXT_AUTO  L=4 */
  "current""\0"                 /* TXT_TCURR  L=7 */
  "feeling""\0"                 /* TXT_TFEEL  L=7 */
  "dew p-t""\0"                 /* TXT_TDEWP  L=7 */
  "\xB0""C""\0"                 /* TXT_TCELS  L=2 */
  "\xB0""F""\0"                 /* TXT_TFAHR  L=2 */
  "K""\0"                       /* TXT_TKELV  L=1 */
  "Pa""\0"                      /* TXT_PPA  L=2 */
  "mmHg""\0"                    /* TXT_PMMHG  L=4 */
  "m""\0"                       /* TXT_PMETER  L=1 */
  "pressure""\0"                /* TXT_PPRES  L=8 */
  "altitude""\0"                /* TXT_PALT  L=8 */
  "delta""\0"                   /* TXT_PDELTA  L=5 */
  "relative""\0"                /* TXT_HREL  L=8 */
  "absolute""\0"                /* TXT_HABS  L=8 */
  "dew p-t""\0"                 /* TXT_HDEWP  L=7 */
  "steam p.""\0"                /* TXT_HSTEAM  L=8 */
  "mV""\0"                      /* TXT_BATTV  L=2 */;

#define TXT_OFF                 (const char *)(strItemMenu + 0)
#define TXT_ON                  (const char *)(strItemMenu + 4)
#define TXT_AUTO                (const char *)(strItemMenu + 7)
#define TXT_TCURR               (const char *)(strItemMenu + 12)
#define TXT_TFEEL               (const char *)(strItemMenu + 20)
#define TXT_TDEWP               (const char *)(strItemMenu + 28)
#define TXT_TCELS               (const char *)(strItemMenu + 36)
#define TXT_TFAHR               (const char *)(strItemMenu + 39)
#define TXT_TKELV               (const char *)(strItemMenu + 42)
#define TXT_PPA                 (const char *)(strItemMenu + 44)
#define TXT_PMMHG               (const char *)(strItemMenu + 47)
#define TXT_PMETER              (const char *)(strItemMenu + 52)
#define TXT_PPRES               (const char *)(strItemMenu + 54)
#define TXT_PALT                (const char *)(strItemMenu + 63)
#define TXT_PDELTA              (const char *)(strItemMenu + 72)
#define TXT_HREL                (const char *)(strItemMenu + 78)
#define TXT_HABS                (const char *)(strItemMenu + 87)
#define TXT_HDEWP               (const char *)(strItemMenu + 96)
#define TXT_HSTEAM              (const char *)(strItemMenu + 104)
#define TXT_BATTV               (const char *)(strItemMenu + 113)

#define TXT_OFF_LENGTH          3
#define TXT_ON_LENGTH           2
#define TXT_AUTO_LENGTH         4
#define TXT_TCURR_LENGTH        7
#define TXT_TFEEL_LENGTH        7
#define TXT_TDEWP_LENGTH        7
#define TXT_TCELS_LENGTH        2
#define TXT_TFAHR_LENGTH        2
#define TXT_TKELV_LENGTH        1
#define TXT_PPA_LENGTH          2
#define TXT_PMMHG_LENGTH        4
#define TXT_PMETER_LENGTH       1
#define TXT_PPRES_LENGTH        8
#define TXT_PALT_LENGTH         8
#define TXT_PDELTA_LENGTH       5
#define TXT_HREL_LENGTH         8
#define TXT_HABS_LENGTH         8
#define TXT_HDEWP_LENGTH        7
#define TXT_HSTEAM_LENGTH       8
#define TXT_BATTV_LENGTH        2

/*******************************************************************************/
/*
#define TXT_ITEM_CURRENT        "current"
#define TXT_ITEM_HOME           "home value"
#define TXT_ITEM_TFEEL          "by feeling"
#define TXT_ITEM_TDEWP          "dew point"
#define TXT_ITEM_PALT           "altitude"
#define TXT_ITEM_PDELTA         "delta"
#define TXT_ITEM_HREL           "relative"
#define TXT_ITEM_HABS           "absolute"
#define TXT_ITEM_HDEWP          "dew point"
#define TXT_ITEM_HSTEAM         "steam pressure"
#define TXT_ITEM_LATTN          "attenuation"
#define TXT_ITEM_DTDATE         "date"
#define TXT_ITEM_DTTIME         "time"
#define TXT_ITEM_DTZONE         "timezone"
#define TXT_ITEM_DTSUMT         "summer time"
#define TXT_ITEM_BATCH          "charge"
#define TXT_ITEM_BATLF          "life time"
*/

const char strItem[] PROGMEM =
  "current""\0"                 /* TXT_ITEM_CURRENT  L=7 */
  "home value""\0"              /* TXT_ITEM_HOME  L=10 */
  "by feeling""\0"              /* TXT_ITEM_TFEEL  L=10 */
  "dew point""\0"               /* TXT_ITEM_TDEWP  L=9 */
  "altitude""\0"                /* TXT_ITEM_PALT  L=8 */
  "delta""\0"                   /* TXT_ITEM_PDELTA  L=5 */
  "relative""\0"                /* TXT_ITEM_HREL  L=8 */
  "absolute""\0"                /* TXT_ITEM_HABS  L=8 */
  "dew point""\0"               /* TXT_ITEM_HDEWP  L=9 */
  "steam pressure""\0"          /* TXT_ITEM_HSTEAM  L=14 */
  "attenuation""\0"             /* TXT_ITEM_LATTN  L=11 */
  "date""\0"                    /* TXT_ITEM_DTDATE  L=4 */
  "time""\0"                    /* TXT_ITEM_DTTIME  L=4 */
  "timezone""\0"                /* TXT_ITEM_DTZONE  L=8 */
  "summer time""\0"             /* TXT_ITEM_DTSUMT  L=11 */
  "charge""\0"                  /* TXT_ITEM_BATCH  L=6 */
  "life time""\0"               /* TXT_ITEM_BATLF  L=9 */;

#define TXT_ITEM_CURRENT        (const char *)(strItem + 0)
#define TXT_ITEM_HOME           (const char *)(strItem + 8)
#define TXT_ITEM_TFEEL          (const char *)(strItem + 19)
#define TXT_ITEM_TDEWP          (const char *)(strItem + 30)
#define TXT_ITEM_PALT           (const char *)(strItem + 40)
#define TXT_ITEM_PDELTA         (const char *)(strItem + 49)
#define TXT_ITEM_HREL           (const char *)(strItem + 55)
#define TXT_ITEM_HABS           (const char *)(strItem + 64)
#define TXT_ITEM_HDEWP          (const char *)(strItem + 73)
#define TXT_ITEM_HSTEAM         (const char *)(strItem + 83)
#define TXT_ITEM_LATTN          (const char *)(strItem + 98)
#define TXT_ITEM_DTDATE         (const char *)(strItem + 110)
#define TXT_ITEM_DTTIME         (const char *)(strItem + 115)
#define TXT_ITEM_DTZONE         (const char *)(strItem + 120)
#define TXT_ITEM_DTSUMT         (const char *)(strItem + 129)
#define TXT_ITEM_BATCH          (const char *)(strItem + 141)
#define TXT_ITEM_BATLF          (const char *)(strItem + 148)

#define TXT_ITEM_CURRENT_LENGTH 7
#define TXT_ITEM_HOME_LENGTH    10
#define TXT_ITEM_TFEEL_LENGTH   10
#define TXT_ITEM_TDEWP_LENGTH   9
#define TXT_ITEM_PALT_LENGTH    8
#define TXT_ITEM_PDELTA_LENGTH  5
#define TXT_ITEM_HREL_LENGTH    8
#define TXT_ITEM_HABS_LENGTH    8
#define TXT_ITEM_HDEWP_LENGTH   9
#define TXT_ITEM_HSTEAM_LENGTH  14
#define TXT_ITEM_LATTN_LENGTH   11
#define TXT_ITEM_DTDATE_LENGTH  4
#define TXT_ITEM_DTTIME_LENGTH  4
#define TXT_ITEM_DTZONE_LENGTH  8
#define TXT_ITEM_DTSUMT_LENGTH  11
#define TXT_ITEM_BATCH_LENGTH   6
#define TXT_ITEM_BATLF_LENGTH   9

/*******************************************************************************/
/*
#define TXT_ITEM_SBRIGH         "brightness"
#define TXT_ITEM_SINV           "inversion"
#define TXT_ITEM_SAUTOF         "auto off"
#define TXT_ITEM_SDISPF         "display off"
#define TXT_ITEM_SALT           "altitude"
#define TXT_ITEM_SLOGPR         "log period"
#define TXT_ITEM_SLOGER         "log erase"
#define TXT_ITEM_SUPDAT         "update"
#define TXT_ITEM_SABOUT         "about"
*/

const char strItemSett[] PROGMEM =
  "brightness""\0"              /* TXT_ITEM_SBRIGH  L=10 */
  "inversion""\0"               /* TXT_ITEM_SINV  L=9 */
  "auto off""\0"                /* TXT_ITEM_SAUTOF  L=8 */
  "display off""\0"             /* TXT_ITEM_SDISPF  L=11 */
  "altitude""\0"                /* TXT_ITEM_SALT  L=8 */
  "log period""\0"              /* TXT_ITEM_SLOGPR  L=10 */
  "log erase""\0"               /* TXT_ITEM_SLOGER  L=9 */
  "update""\0"                  /* TXT_ITEM_SUPDAT  L=6 */
  "about""\0"                   /* TXT_ITEM_SABOUT  L=5 */;

#define TXT_ITEM_SBRIGH         (const char *)(strItemSett + 0)
#define TXT_ITEM_SINV           (const char *)(strItemSett + 11)
#define TXT_ITEM_SAUTOF         (const char *)(strItemSett + 21)
#define TXT_ITEM_SDISPF         (const char *)(strItemSett + 30)
#define TXT_ITEM_SALT           (const char *)(strItemSett + 42)
#define TXT_ITEM_SLOGPR         (const char *)(strItemSett + 51)
#define TXT_ITEM_SLOGER         (const char *)(strItemSett + 62)
#define TXT_ITEM_SUPDAT         (const char *)(strItemSett + 72)
#define TXT_ITEM_SABOUT         (const char *)(strItemSett + 79)

#define TXT_ITEM_SBRIGH_LENGTH  10
#define TXT_ITEM_SINV_LENGTH    9
#define TXT_ITEM_SAUTOF_LENGTH  8
#define TXT_ITEM_SDISPF_LENGTH  11
#define TXT_ITEM_SALT_LENGTH    8
#define TXT_ITEM_SLOGPR_LENGTH  10
#define TXT_ITEM_SLOGER_LENGTH  9
#define TXT_ITEM_SUPDAT_LENGTH  6
#define TXT_ITEM_SABOUT_LENGTH  5

/*******************************************************************************/
/*
#define TXT_HDR_TEMP            "TEMPERATURE"
#define TXT_HDR_BATT            "BATTERY"
#define TXT_HDR_HUM             "HUMIDITY"
#define TXT_HDR_TIME            "DATE & TIME"
#define TXT_HDR_PRES            "PRESSURE"
#define TXT_HDR_SETT            "SETTINGS"
#define TXT_HDR_LIGHT           "ILLUMINANCE"
*/

const char strHeader[] PROGMEM =
  "TEMPERATURE""\0"             /* TXT_HDR_TEMP  L=11 */
  "BATTERY""\0"                 /* TXT_HDR_BATT  L=7 */
  "HUMIDITY""\0"                /* TXT_HDR_HUM  L=8 */
  "DATE & TIME""\0"             /* TXT_HDR_TIME  L=11 */
  "PRESSURE""\0"                /* TXT_HDR_PRES  L=8 */
  "SETTINGS""\0"                /* TXT_HDR_SETT  L=8 */
  "ILLUMINANCE""\0"             /* TXT_HDR_LIGHT  L=11 */;

#define TXT_HDR_TEMP            (const char *)(strHeader + 0)
#define TXT_HDR_BATT            (const char *)(strHeader + 12)
#define TXT_HDR_HUM             (const char *)(strHeader + 20)
#define TXT_HDR_TIME            (const char *)(strHeader + 29)
#define TXT_HDR_PRES            (const char *)(strHeader + 41)
#define TXT_HDR_SETT            (const char *)(strHeader + 50)
#define TXT_HDR_LIGHT           (const char *)(strHeader + 59)

#define TXT_HDR_TEMP_LENGTH     11
#define TXT_HDR_BATT_LENGTH     7
#define TXT_HDR_HUM_LENGTH      8
#define TXT_HDR_TIME_LENGTH     11
#define TXT_HDR_PRES_LENGTH     8
#define TXT_HDR_SETT_LENGTH     8
#define TXT_HDR_LIGHT_LENGTH    11

/*******************************************************************************/
// max length = unlimited, check it on real screen
#define TXT_LOG_WARN1           "Do not connect"
#define TXT_LOG_WARN2           "to the host!"
#define TXT_LOG_WARN3           "Please wait ~30 s"
#define TXT_PCC_INFO            "Connected to host"
/*******************************************************************************/
#endif



/*******************************************************************************/
/*******************************************************************************/

#if (LANGUAGE == 1)     // ���� ���������� �������

/*******************************************************************************/
/*
#define TXT_DAY_1               "��"
#define TXT_DAY_2               "��"
#define TXT_DAY_3               "��"
#define TXT_DAY_4               "��"
#define TXT_DAY_5               "��"
#define TXT_DAY_6               "��"
#define TXT_DAY_7               "��"
*/
const char strWeekDay[] PROGMEM =
  "��""\0"                      /* TXT_DAY_1  L=2 */
  "��""\0"                      /* TXT_DAY_2  L=2 */
  "��""\0"                      /* TXT_DAY_3  L=2 */
  "��""\0"                      /* TXT_DAY_4  L=2 */
  "��""\0"                      /* TXT_DAY_5  L=2 */
  "��""\0"                      /* TXT_DAY_6  L=2 */
  "��""\0"                      /* TXT_DAY_7  L=2 */;

#define TXT_DAY_1               (const char *)(strWeekDay + 0)
#define TXT_DAY_2               (const char *)(strWeekDay + 3)
#define TXT_DAY_3               (const char *)(strWeekDay + 6)
#define TXT_DAY_4               (const char *)(strWeekDay + 9)
#define TXT_DAY_5               (const char *)(strWeekDay + 12)
#define TXT_DAY_6               (const char *)(strWeekDay + 15)
#define TXT_DAY_7               (const char *)(strWeekDay + 18)

#define TXT_DAY_1_LENGTH        2
#define TXT_DAY_2_LENGTH        2
#define TXT_DAY_3_LENGTH        2
#define TXT_DAY_4_LENGTH        2
#define TXT_DAY_5_LENGTH        2
#define TXT_DAY_6_LENGTH        2
#define TXT_DAY_7_LENGTH        2

/*******************************************************************************/
/*
#define TXT_DELIM_SPACE         " "
#define TXT_DELIM_DOT           "."
#define TXT_DELIM_COLON         ":"
#define TXT_DELIM_LUX           "��"
#define TXT_DELIM_UTC           "UTC "
#define TXT_DELIM_HOUR          "� "
#define TXT_DELIM_MINUTE        "�"
#define TXT_DELIM_MINSP         "� "
#define TXT_DELIM_METER         "�"
#define TXT_DELIM_PRCNT         "%"
#define TXT_DELIM_GMCUBE        "�/�\x9A"
#define TXT_DELIM_SECOND        "�"
*/

const char strDelimiter[] PROGMEM =
  " ""\0"                       /* TXT_DELIM_SPACE  L=1 */
  ".""\0"                       /* TXT_DELIM_DOT  L=1 */
  ":""\0"                       /* TXT_DELIM_COLON  L=1 */
  "��""\0"                      /* TXT_DELIM_LUX  L=2 */
  "UTC ""\0"                    /* TXT_DELIM_UTC  L=4 */
  "� ""\0"                      /* TXT_DELIM_HOUR  L=2 */
  "�""\0"                       /* TXT_DELIM_MINUTE  L=1 */
  "� ""\0"                      /* TXT_DELIM_MINSP  L=2 */
  "�""\0"                       /* TXT_DELIM_METER  L=1 */
  "%""\0"                       /* TXT_DELIM_PRCNT  L=1 */
  "�/�\x9A""\0"                 /* TXT_DELIM_GMCUBE  L=4 */
  "�""\0"                       /* TXT_DELIM_SECOND  L=1 */;

#define TXT_DELIM_SPACE         (const char *)(strDelimiter + 0)
#define TXT_DELIM_DOT           (const char *)(strDelimiter + 2)
#define TXT_DELIM_COLON         (const char *)(strDelimiter + 4)
#define TXT_DELIM_LUX           (const char *)(strDelimiter + 6)
#define TXT_DELIM_UTC           (const char *)(strDelimiter + 9)
#define TXT_DELIM_HOUR          (const char *)(strDelimiter + 14)
#define TXT_DELIM_MINUTE        (const char *)(strDelimiter + 17)
#define TXT_DELIM_MINSP         (const char *)(strDelimiter + 19)
#define TXT_DELIM_METER         (const char *)(strDelimiter + 22)
#define TXT_DELIM_PRCNT         (const char *)(strDelimiter + 24)
#define TXT_DELIM_GMCUBE        (const char *)(strDelimiter + 26)
#define TXT_DELIM_SECOND        (const char *)(strDelimiter + 31)
#define TXT_DELIM_NONE          (const char *)(strDelimiter + TXT_DELIM_SPACE_LENGTH)

#define TXT_DELIM_SPACE_LENGTH  1
#define TXT_DELIM_DOT_LENGTH    1
#define TXT_DELIM_COLON_LENGTH  1
#define TXT_DELIM_LUX_LENGTH    2
#define TXT_DELIM_UTC_LENGTH    4
#define TXT_DELIM_HOUR_LENGTH   2
#define TXT_DELIM_MINUTE_LENGTH 1
#define TXT_DELIM_MINSP_LENGTH  2
#define TXT_DELIM_METER_LENGTH  1
#define TXT_DELIM_PRCNT_LENGTH  1
#define TXT_DELIM_GMCUBE_LENGTH 4
#define TXT_DELIM_SECOND_LENGTH 1

/*******************************************************************************/
/*
#define TXT_OFF                 "����"
#define TXT_ON                  "���"
#define TXT_AUTO                "����"
#define TXT_TCURR               "������"
#define TXT_TFEEL               "��������"
#define TXT_TDEWP               "�. ����"
#define TXT_TCELS               "\xB0""C"
#define TXT_TFAHR               "\xB0""F"
#define TXT_TKELV               "K"
#define TXT_PPA                 "��"
#define TXT_PMMHG               "����"
#define TXT_PMETER              "�"
#define TXT_PPRES               "��������"
#define TXT_PALT                "������"
#define TXT_PDELTA              "��������"
#define TXT_HREL                "�������."
#define TXT_HABS                "�������."
#define TXT_HDEWP               "�. ����"
#define TXT_HSTEAM              "����. ��"
#define TXT_BATTV               "��"
*/

const char strItemMenu[] PROGMEM =
  "����""\0"                    /* TXT_OFF  L=4 */
  "���""\0"                     /* TXT_ON  L=3 */
  "����""\0"                    /* TXT_AUTO  L=4 */
  "������""\0"                  /* TXT_TCURR  L=6 */
  "��������""\0"                /* TXT_TFEEL  L=8 */
  "�. ����""\0"                 /* TXT_TDEWP  L=7 */
  "\xB0""C""\0"                 /* TXT_TCELS  L=2 */
  "\xB0""F""\0"                 /* TXT_TFAHR  L=2 */
  "K""\0"                       /* TXT_TKELV  L=1 */
  "��""\0"                      /* TXT_PPA  L=2 */
  "����""\0"                    /* TXT_PMMHG  L=4 */
  "�""\0"                       /* TXT_PMETER  L=1 */
  "��������""\0"                /* TXT_PPRES  L=8 */
  "������""\0"                  /* TXT_PALT  L=6 */
  "��������""\0"                /* TXT_PDELTA  L=8 */
  "�������.""\0"                /* TXT_HREL  L=8 */
  "�������.""\0"                /* TXT_HABS  L=8 */
  "�. ����""\0"                 /* TXT_HDEWP  L=7 */
  "����. ��""\0"                /* TXT_HSTEAM  L=8 */
  "��""\0"                      /* TXT_BATTV  L=2 */;

#define TXT_OFF                 (const char *)(strItemMenu + 0)
#define TXT_ON                  (const char *)(strItemMenu + 5)
#define TXT_AUTO                (const char *)(strItemMenu + 9)
#define TXT_TCURR               (const char *)(strItemMenu + 14)
#define TXT_TFEEL               (const char *)(strItemMenu + 21)
#define TXT_TDEWP               (const char *)(strItemMenu + 30)
#define TXT_TCELS               (const char *)(strItemMenu + 38)
#define TXT_TFAHR               (const char *)(strItemMenu + 41)
#define TXT_TKELV               (const char *)(strItemMenu + 44)
#define TXT_PPA                 (const char *)(strItemMenu + 46)
#define TXT_PMMHG               (const char *)(strItemMenu + 49)
#define TXT_PMETER              (const char *)(strItemMenu + 54)
#define TXT_PPRES               (const char *)(strItemMenu + 56)
#define TXT_PALT                (const char *)(strItemMenu + 65)
#define TXT_PDELTA              (const char *)(strItemMenu + 72)
#define TXT_HREL                (const char *)(strItemMenu + 81)
#define TXT_HABS                (const char *)(strItemMenu + 90)
#define TXT_HDEWP               (const char *)(strItemMenu + 99)
#define TXT_HSTEAM              (const char *)(strItemMenu + 107)
#define TXT_BATTV               (const char *)(strItemMenu + 116)

#define TXT_OFF_LENGTH          4
#define TXT_ON_LENGTH           3
#define TXT_AUTO_LENGTH         4
#define TXT_TCURR_LENGTH        6
#define TXT_TFEEL_LENGTH        8
#define TXT_TDEWP_LENGTH        7
#define TXT_TCELS_LENGTH        2
#define TXT_TFAHR_LENGTH        2
#define TXT_TKELV_LENGTH        1
#define TXT_PPA_LENGTH          2
#define TXT_PMMHG_LENGTH        4
#define TXT_PMETER_LENGTH       1
#define TXT_PPRES_LENGTH        8
#define TXT_PALT_LENGTH         6
#define TXT_PDELTA_LENGTH       8
#define TXT_HREL_LENGTH         8
#define TXT_HABS_LENGTH         8
#define TXT_HDEWP_LENGTH        7
#define TXT_HSTEAM_LENGTH       8
#define TXT_BATTV_LENGTH        2

/*******************************************************************************/
/*
#define TXT_ITEM_CURRENT        "������"
#define TXT_ITEM_HOME           "����������"
#define TXT_ITEM_TFEEL          "�� ��������"
#define TXT_ITEM_TDEWP          "����� ����"
#define TXT_ITEM_PALT           "������"
#define TXT_ITEM_PDELTA         "��������"
#define TXT_ITEM_HREL           "�������������"
#define TXT_ITEM_HABS           "����������"
#define TXT_ITEM_HDEWP          "����� ����"
#define TXT_ITEM_HSTEAM         "����. H20 ����"
#define TXT_ITEM_LATTN          "����������"
#define TXT_ITEM_DTDATE         "����"
#define TXT_ITEM_DTTIME         "�����"
#define TXT_ITEM_DTZONE         "������� ����"
#define TXT_ITEM_DTSUMT         "������ �����"
#define TXT_ITEM_BATCH          "�����"
#define TXT_ITEM_BATLF          "����� ������"
*/

const char strItem[] PROGMEM =
  "������""\0"                  /* TXT_ITEM_CURRENT  L=6 */
  "����������""\0"              /* TXT_ITEM_HOME  L=10 */
  "�� ��������""\0"             /* TXT_ITEM_TFEEL  L=11 */
  "����� ����""\0"              /* TXT_ITEM_TDEWP  L=10 */
  "������""\0"                  /* TXT_ITEM_PALT  L=6 */
  "��������""\0"                /* TXT_ITEM_PDELTA  L=8 */
  "�������������""\0"           /* TXT_ITEM_HREL  L=13 */
  "����������""\0"              /* TXT_ITEM_HABS  L=10 */
  "����� ����""\0"              /* TXT_ITEM_HDEWP  L=10 */
  "����. H20 ����""\0"          /* TXT_ITEM_HSTEAM  L=14 */
  "����������""\0"              /* TXT_ITEM_LATTN  L=10 */
  "����""\0"                    /* TXT_ITEM_DTDATE  L=4 */
  "�����""\0"                   /* TXT_ITEM_DTTIME  L=5 */
  "������� ����""\0"            /* TXT_ITEM_DTZONE  L=12 */
  "������ �����""\0"            /* TXT_ITEM_DTSUMT  L=12 */
  "�����""\0"                   /* TXT_ITEM_BATCH  L=5 */
  "����� ������""\0"            /* TXT_ITEM_BATLF  L=12 */;

#define TXT_ITEM_CURRENT        (const char *)(strItem + 0)
#define TXT_ITEM_HOME           (const char *)(strItem + 7)
#define TXT_ITEM_TFEEL          (const char *)(strItem + 18)
#define TXT_ITEM_TDEWP          (const char *)(strItem + 30)
#define TXT_ITEM_PALT           (const char *)(strItem + 41)
#define TXT_ITEM_PDELTA         (const char *)(strItem + 48)
#define TXT_ITEM_HREL           (const char *)(strItem + 57)
#define TXT_ITEM_HABS           (const char *)(strItem + 71)
#define TXT_ITEM_HDEWP          (const char *)(strItem + 82)
#define TXT_ITEM_HSTEAM         (const char *)(strItem + 93)
#define TXT_ITEM_LATTN          (const char *)(strItem + 108)
#define TXT_ITEM_DTDATE         (const char *)(strItem + 119)
#define TXT_ITEM_DTTIME         (const char *)(strItem + 124)
#define TXT_ITEM_DTZONE         (const char *)(strItem + 130)
#define TXT_ITEM_DTSUMT         (const char *)(strItem + 143)
#define TXT_ITEM_BATCH          (const char *)(strItem + 156)
#define TXT_ITEM_BATLF          (const char *)(strItem + 162)

#define TXT_ITEM_CURRENT_LENGTH 6
#define TXT_ITEM_HOME_LENGTH    10
#define TXT_ITEM_TFEEL_LENGTH   11
#define TXT_ITEM_TDEWP_LENGTH   10
#define TXT_ITEM_PALT_LENGTH    6
#define TXT_ITEM_PDELTA_LENGTH  8
#define TXT_ITEM_HREL_LENGTH    13
#define TXT_ITEM_HABS_LENGTH    10
#define TXT_ITEM_HDEWP_LENGTH   10
#define TXT_ITEM_HSTEAM_LENGTH  14
#define TXT_ITEM_LATTN_LENGTH   10
#define TXT_ITEM_DTDATE_LENGTH  4
#define TXT_ITEM_DTTIME_LENGTH  5
#define TXT_ITEM_DTZONE_LENGTH  12
#define TXT_ITEM_DTSUMT_LENGTH  12
#define TXT_ITEM_BATCH_LENGTH   5
#define TXT_ITEM_BATLF_LENGTH   12



/*******************************************************************************/
/*
#define TXT_ITEM_SBRIGH         "�������"
#define TXT_ITEM_SINV           "��������"
#define TXT_ITEM_SAUTOF         "��������������"
#define TXT_ITEM_SDISPF         "����. �������"
#define TXT_ITEM_SALT           "������ �������"
#define TXT_ITEM_SLOGPR         "������ ����."
#define TXT_ITEM_SLOGER         "������� ����."
#define TXT_ITEM_SUPDAT         "����������"
#define TXT_ITEM_SABOUT         "����������"
*/

const char strItemSett[] PROGMEM =
  "�������""\0"                 /* TXT_ITEM_SBRIGH  L=7 */
  "��������""\0"                /* TXT_ITEM_SINV  L=8 */
  "��������������""\0"          /* TXT_ITEM_SAUTOF  L=14 */
  "����. �������""\0"           /* TXT_ITEM_SDISPF  L=13 */
  "������ �������""\0"          /* TXT_ITEM_SALT  L=14 */
  "������ ����.""\0"            /* TXT_ITEM_SLOGPR  L=12 */
  "������� ����.""\0"           /* TXT_ITEM_SLOGER  L=13 */
  "����������""\0"              /* TXT_ITEM_SUPDAT  L=10 */
  "����������""\0"              /* TXT_ITEM_SABOUT  L=10 */;

#define TXT_ITEM_SBRIGH         (const char *)(strItemSett + 0)
#define TXT_ITEM_SINV           (const char *)(strItemSett + 8)
#define TXT_ITEM_SAUTOF         (const char *)(strItemSett + 17)
#define TXT_ITEM_SDISPF         (const char *)(strItemSett + 32)
#define TXT_ITEM_SALT           (const char *)(strItemSett + 46)
#define TXT_ITEM_SLOGPR         (const char *)(strItemSett + 61)
#define TXT_ITEM_SLOGER         (const char *)(strItemSett + 74)
#define TXT_ITEM_SUPDAT         (const char *)(strItemSett + 88)
#define TXT_ITEM_SABOUT         (const char *)(strItemSett + 99)

#define TXT_ITEM_SBRIGH_LENGTH  7
#define TXT_ITEM_SINV_LENGTH    8
#define TXT_ITEM_SAUTOF_LENGTH  14
#define TXT_ITEM_SDISPF_LENGTH  13
#define TXT_ITEM_SALT_LENGTH    14
#define TXT_ITEM_SLOGPR_LENGTH  12
#define TXT_ITEM_SLOGER_LENGTH  13
#define TXT_ITEM_SUPDAT_LENGTH  10
#define TXT_ITEM_SABOUT_LENGTH  10

/*******************************************************************************/
/*
#define TXT_HDR_TEMP            "�����������"
#define TXT_HDR_BATT            "�������"
#define TXT_HDR_HUM             "���������"
#define TXT_HDR_TIME            "���� � �����"
#define TXT_HDR_PRES            "��������"
#define TXT_HDR_SETT            "���������"
#define TXT_HDR_LIGHT           "������������"
*/

const char strHeader[] PROGMEM =
  "�����������""\0"             /* TXT_HDR_TEMP  L=11 */
  "�������""\0"                 /* TXT_HDR_BATT  L=7 */
  "���������""\0"               /* TXT_HDR_HUM  L=9 */
  "���� � �����""\0"            /* TXT_HDR_TIME  L=12 */
  "��������""\0"                /* TXT_HDR_PRES  L=8 */
  "���������""\0"               /* TXT_HDR_SETT  L=9 */
  "������������""\0"            /* TXT_HDR_LIGHT  L=12 */;

#define TXT_HDR_TEMP            (const char *)(strHeader + 0)
#define TXT_HDR_BATT            (const char *)(strHeader + 12)
#define TXT_HDR_HUM             (const char *)(strHeader + 20)
#define TXT_HDR_TIME            (const char *)(strHeader + 30)
#define TXT_HDR_PRES            (const char *)(strHeader + 43)
#define TXT_HDR_SETT            (const char *)(strHeader + 52)
#define TXT_HDR_LIGHT           (const char *)(strHeader + 62)

#define TXT_HDR_TEMP_LENGTH     11
#define TXT_HDR_BATT_LENGTH     7
#define TXT_HDR_HUM_LENGTH      9
#define TXT_HDR_TIME_LENGTH     12
#define TXT_HDR_PRES_LENGTH     8
#define TXT_HDR_SETT_LENGTH     9
#define TXT_HDR_LIGHT_LENGTH    12

/*******************************************************************************/
// max length = unlimited, check it on real screen
#define TXT_LOG_WARN1           "�� �������������"
#define TXT_LOG_WARN2           "� ����� / �� !"
#define TXT_LOG_WARN3           "��������� ~30 �"
#define TXT_PCC_INFO            "��������� � �����"
/*******************************************************************************/
#endif



/*******************************************************************************/
/*******************************************************************************/

const char strLogWarning1[] PROGMEM = TXT_LOG_WARN1;
const char strLogWarning2[] PROGMEM = TXT_LOG_WARN2;
const char strLogWarning3[] PROGMEM = TXT_LOG_WARN3;
const char strHostConect [] PROGMEM = TXT_PCC_INFO;

const char* const  daysOfWeek[] PROGMEM = {TXT_DAY_1, TXT_DAY_2, TXT_DAY_3, TXT_DAY_4, TXT_DAY_5, TXT_DAY_6, TXT_DAY_7};

#endif /* TXT_LABELS_H */
