#include "buttons.h"
#include "includes.h"

/*******************************************************************************/

#define FOR_ALL_BTN for(uint8_t i = 0; i < _count; i++)


void CustomButtons::begin(uint8_t debounce_ms) {
  _count = 0;
}

void CustomButtons::reset() {
  FOR_ALL_BTN {
    _btn[i].pinState = _btn[i].freeState;
    _btn[i].activated = 0;
    _btn[i].click   = 0;
    _btn[i].press   = 0;
    _btn[i].hold    = 0;
  }
  //  poll();
}

void CustomButtons::poll() {
  uint16_t t = millis();
  FOR_ALL_BTN {
    uint8_t f = _btn[i].freeState;
    uint8_t p = _btn[i].pinState;
    uint8_t s = _btn[i].pinState = _digitalRead(_btn[i].pin);

    if (s != f) {

      if (p == f) {
        _btn[i].time    = t;
        _btn[i].click   = 0;
        _btn[i].press   = 0;
        _btn[i].hold    = 0;
        _btn[i].activated = 1;
        continue;

      } else {
        if (_btn[i].activated)
          if (t - _btn[i].time > _pressTime) {
            _btn[i].activated = 0;
            _btn[i].press     = !_btn[i].hold;
            _btn[i].hold      = 1;
          }
      }

    } else {
      if (_btn[i].activated && !_btn[i].hold)
        if (t - _btn[i].time > _debounce) {
          _btn[i].click     = _btn[i].activated;
          _btn[i].activated = 0;
        }

      _btn[i].hold = 0;
    }
  }
}

void CustomButtons::addButton(uint8_t pin, uint8_t code, uint8_t freeState) {
  if (_count >= BUTTONS_MAX) return;
  _btn[_count].pin          = pin;
  _btn[_count].code         = code;
  _btn[_count].freeState    = freeState;
  _btn[_count].activated    = 0;

  if (freeState == BTN_FREE_IS_VCC)
    _pinMode(pin, INPUT_PULLUP);
  else
    _pinMode(pin, INPUT);

  _count++;
}

uint8_t CustomButtons::clicked() {
  FOR_ALL_BTN
  if (_btn[i].click) {
    _btn[i].click = 0;
    return _btn[i].code;
  }

  return BUTTON_NONE;
}

uint8_t CustomButtons::pressed() {
  FOR_ALL_BTN
  if (_btn[i].press) {
    _btn[i].press = 0;
    return _btn[i].code;
  }

  return BUTTON_NONE;
}

uint8_t CustomButtons::holded() {
  FOR_ALL_BTN
  if (_btn[i].hold) {
    return _btn[i].code;
  }

  return BUTTON_NONE;
}

uint8_t CustomButtons::getState(uint8_t pin) {
  FOR_ALL_BTN
  if (_btn[i].pin == pin) {

    if (_btn[i].click) {
      _btn[i].click = 0;
      return BTN_ACTION_CLICK;
    }

    if (_btn[i].press) {
      _btn[i].press = 0;
      return BTN_ACTION_PRESS;
    }

    if (_btn[i].hold)
      return BTN_ACTION_HOLD;

    return BTN_ACTION_WAIT;
  }
}

uint8_t CustomButtons::isActivated() {
  FOR_ALL_BTN
  if (_btn[i].click || _btn[i].press || _btn[i].hold) return 1;
  return 0;
}

void CustomButtons::waitForRelease() {
  while (holded() != BUTTON_NONE) poll();
  reset();
}
