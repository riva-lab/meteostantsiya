#include "log.h"
#include "connections.h"
#include "settings.h"
#include "devices.h"
#include "includes.h"

/*******************************************************************************/

MeteoLogRecord_t record;

/*******************************************************************************/

void logErase() {
  setting.logAddress = 0;
  flash.wakeUp();
  flash.eraseAll();
  while (flash.isBusy());
  flash.sleep();
}

void logWrite() {
  //  utc.update();
  t_datetime        tc  = utc.getDateTime();
  static t_datetime t   = 0;
  uint16_t          p   = setting.logPeriodM * 60 + setting.logPeriodS;

  if ((((uint32_t)tc % p) != 0) || ((uint32_t)tc == t)) return;
  t = tc;
                     
  record.bit_st         = (utc.getSummerTime() != 0);
  record.bit_battery    = (uint8_t )(liIonBatteryPercent());
  record.st_shift       = (int8_t  )(utc.getSummerTimeDelta());
  record.timezone       = (int16_t )(utc.getZone());
  record.datetime       = (uint32_t)(tc);
  record.humidity       = (uint8_t )(mc.getHumidity());
  record.pressure       = (uint32_t)(mc.getPressure());
  record.temperature    = (int16_t )(mc.getTemperature() * 100.0);
  record.lux            = (uint16_t)(luxValue);

  flash.wakeUp();

  // проверка, пуст ли сектор перед записью первого лога в нем
  // если сектор был не пуст - стираем
  if ((setting.logAddress % W25Q_SIZE_SECTOR) == 0)
    for (uint16_t i = 0; i < W25Q_SIZE_SECTOR; i++) {
      flash.setAddress(setting.logAddress + i);
      if (flash.read() != W25Q_ERASE_VALUE) {
        flash.eraseSector();
        break;
      }
    }
      
  // пишем запись лога по текущему адресу
  flash.setAddress(setting.logAddress);
  flash.write((uint8_t *)(&record), sizeof(record));
  setting.logAddress += sizeof(record);

  // зацикливание: после записи всей памяти, данные начинают записываться сначала
  if (setting.logAddress > (1UL << flash.getCapacity()))
    setting.logAddress = 0;

  flash.sleep();
}
