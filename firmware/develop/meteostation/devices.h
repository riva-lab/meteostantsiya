#ifndef DEVICES_H
#define DEVICES_H

#include "connections.h"
#include "includes.h"

/*******************************************************************************/

extern SimpleSSD1306        disp;
extern BH1750FVI            myLux;
extern SimpleBME280         bme280;
extern SimpleSPIFlash       flash;
extern CustomButtons        btn;
extern SimpleMeteoCalc      mc;

extern embedDateTime        utc;
extern t_datetime_struct    xt;
extern uint8_t              z, st;
extern float                luxValue;

/*******************************************************************************/

#define LUXMETER_ADDRESS    0x23
#define TIME_MEASURING      2000    // [мс] период опроса погодных датчиков
#define TIME_BATTERY        2000    // [мс] период обновления уровня заряда АБ

#define SWITCH_DROPDOWN     140     // [мВ] падение напряжения на ключе схемы включения
#define ADC_UREF_CALIBRATED 1104    // [мВ] калибровка Uref (1100 мВ) для замера напряжения
#define LIION_VOLTAGE_100   4200    // [мВ] напряжение 100% заряженной Li-Ion АБ
#define LIION_VOLTAGE_80    3950    // [мВ] напряжение 80%  заряженной Li-Ion АБ
#define LIION_VOLTAGE_60    3850    // [мВ] напряжение 60%  заряженной Li-Ion АБ
#define LIION_VOLTAGE_40    3750    // [мВ] напряжение 40%  заряженной Li-Ion АБ
#define LIION_VOLTAGE_20    3700    // [мВ] напряжение 20%  заряженной Li-Ion АБ
#define LIION_VOLTAGE_0     2800    // [мВ] напряжение 0%   заряженной Li-Ion АБ

#define BATTERY_LOW         15      // [%]    нижний уровень заряда аккумулятора
#define BATTERY_CAPACITY    350     // [мА*ч] емкость установленного аккумулятора
#define CONSUMPTION_SLEEP   2.7     // [мА]   ток потребления в режиме сна
#define CONSUMPTION_ACTIVE  18      // [мА]   ток потребления в активном режиме (дисп. неинверсный)

#define AUTOINVERT_LUX_VALUE 500    // [люкс] среднее значение освещенности, выше которой дисплей инвертируется

#define UART_BAUDRATE       115200  // скорость соединения с хостом по UART

/*******************************************************************************/

void        commonInit();               // общая инициализация
void        measureWeatherParameters(); // опрос датчиков и запись в лог

// управление питанием и энергосбережением
void        powerState(uint8_t enable);
uint8_t     powerIsCharging();
void        powerOffTimersReset(uint8_t reset);
uint8_t     isSleepMode();
void        waitInSleep();

// замер напряжения питания
uint16_t    getSystemVoltage();
uint8_t     liIonBatteryPercent();

// связь с хостом по UART
uint8_t     connectHostUpdate();


#endif /* DEVICES_H */
