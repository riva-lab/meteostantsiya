#ifndef PAR_LIST_H
#define PAR_LIST_H

#include "sett_item.h"
#include "includes.h"
#include "devices.h"
#include "handlers.h"

#include <stdint.h>


/*******************************************************************************/
/*                               Строковые наборы                              */
/*******************************************************************************/

// список для параметра "ВКЛ/ВЫКЛ/АВТО"
const char* const strOffOnAuto      [] PROGMEM = { TXT_OFF, TXT_ON, TXT_AUTO };

// список для параметра "ЕДИНИЦА ТЕМП."
const char* const strTempUnit       [] PROGMEM = { TXT_TCELS, TXT_TFAHR, TXT_TKELV };

// список для параметра "ЗНАЧЕНИЕ ТЕМП."
const char* const strTempValue      [] PROGMEM = { TXT_TCURR, TXT_TFEEL, TXT_TDEWP };

// список для параметра "ЕДИНИЦА ДАВЛ."
const char* const strPresUnit       [] PROGMEM = { TXT_PPA, TXT_PMMHG };

// список для параметра "ЕДИНИЦА РАЗНОСТИ ДАВЛ."
const char* const strPresDeltaUnit  [] PROGMEM = { TXT_PPA, TXT_PMMHG, TXT_PMETER };

// список для параметра "ЗНАЧЕНИЕ ДАВЛ."
const char* const strPresValue      [] PROGMEM = { TXT_PPRES, TXT_PALT, TXT_PDELTA };

// список для параметра "ЗНАЧЕНИЕ ВЛАЖН."
const char* const strHumValue       [] PROGMEM = { TXT_HREL, TXT_HABS, TXT_HDEWP, TXT_HSTEAM };

// список для параметра "ЕДИНИЦА БАТ."
const char* const strBattUnit       [] PROGMEM = { TXT_DELIM_PRCNT, TXT_BATTV };


/*******************************************************************************/
/*                                   ДИАЛОГИ                                   */
/*******************************************************************************
  ТИП  | тип
  СТР  | указатель на начало массива строк
  МИН  | мин.значение
  МАКС | макс.значение
  ШАГ  | шаг изменения параметра
  ЗНАК | кол-во знаков после запятой
  ОБР  | обработчик получения/записи параметра
 *******************************************************************************/

#define DEFINE_DIALOG(name)     const dialog_t name PROGMEM = {
#define DEFINE_DIALOG_END       };

DEFINE_DIALOG(dlgTempCurrent) {
  //  ТИП               СТР                 МИН МАКС    ШАГ ЗНАК    ОБР
  { PARAM_TYPE_NUM,     TXT_DELIM_SPACE,    0,  0,      0,  2,      tempHandler     },
  { PARAM_TYPE_STR,     strTempUnit,        0,  2,      1,  0,      tempUnitHandler },
  PARAM_EMPTY()
} DEFINE_DIALOG_END

DEFINE_DIALOG(dlgTempFeel) {
  { PARAM_TYPE_NUM,     TXT_DELIM_SPACE,    0,  0,      0,  2,      tempFeelHandler },
  { PARAM_TYPE_STR,     strTempUnit,        0,  2,      1,  0,      tempUnitHandler },
  PARAM_EMPTY()
} DEFINE_DIALOG_END

DEFINE_DIALOG(dlgTempDew) {
  { PARAM_TYPE_NUM,     TXT_DELIM_SPACE,    0,  0,      0,  2,      tempDewHandler },
  { PARAM_TYPE_STR,     strTempUnit,        0,  2,      1,  0,      tempUnitHandler },
  PARAM_EMPTY()
} DEFINE_DIALOG_END

DEFINE_DIALOG(dlgTempValue) {
  //  ТИП               СТР                 МИН МАКС    ШАГ ЗНАК    ОБР
  { PARAM_TYPE_STR,     strTempValue,       0,  2,      1,  0,      tempValueHandler },
  PARAM_EMPTY(),
  PARAM_EMPTY()
} DEFINE_DIALOG_END

DEFINE_DIALOG(dlgBattery) {
  { PARAM_TYPE_NUM,     TXT_DELIM_SPACE,    0,  0,      0,  0,      batteryHandler },
  { PARAM_TYPE_STR,     strBattUnit,        0,  1,      1,  0,      battUnitHandler },
  PARAM_EMPTY()
} DEFINE_DIALOG_END

DEFINE_DIALOG(dlgBatteryLife) {
  PARAM_LABEL(TXT_DELIM_SPACE),
  { PARAM_TYPE_NUM,     TXT_DELIM_NONE,     0,  0,      0,  0,      battLifeHandler },
  PARAM_LABEL(TXT_DELIM_HOUR)
} DEFINE_DIALOG_END

DEFINE_DIALOG(dlgHumidity) {
  //  ТИП               СТР                 МИН МАКС    ШАГ ЗНАК    ОБР
  { PARAM_TYPE_NUM,     TXT_DELIM_SPACE,    0,  0,      0,  0,      humidityHandler },
  PARAM_LABEL(TXT_DELIM_PRCNT),
  PARAM_EMPTY()
} DEFINE_DIALOG_END

DEFINE_DIALOG(dlgHumidityAbs) {
  { PARAM_TYPE_NUM,     TXT_DELIM_SPACE,    0,  0,      0,  1,      humAbsHandler },
  PARAM_LABEL(TXT_DELIM_GMCUBE),
  PARAM_EMPTY()
} DEFINE_DIALOG_END

DEFINE_DIALOG(dlgSteamPres) {
  { PARAM_TYPE_NUM,     TXT_DELIM_SPACE,    0,  0,      0,  0,      steamPresHandler },
  PARAM_LABEL(TXT_PPA),
  PARAM_EMPTY()
} DEFINE_DIALOG_END

DEFINE_DIALOG(dlgHumValue) {
  //  ТИП               СТР                 МИН МАКС    ШАГ ЗНАК    ОБР
  { PARAM_TYPE_STR,     strHumValue,        0,  3,      1,  0,      humValueHandler },
  PARAM_EMPTY(),
  PARAM_EMPTY()
} DEFINE_DIALOG_END

DEFINE_DIALOG(dlgDate) {
  { PARAM_TYPE_NUM2,    TXT_DELIM_DOT,      0,  99,     1,  0,      dateYearHandler },
  { PARAM_TYPE_NUM2,    TXT_DELIM_DOT,      1,  12,     1,  0,      dateMonHandler  },
  { PARAM_TYPE_NUM2,    TXT_DELIM_NONE,     1,  31,     1,  0,      dateDayHandler  },
} DEFINE_DIALOG_END

DEFINE_DIALOG(dlgTime) {
  { PARAM_TYPE_NUM,     TXT_DELIM_COLON,    0,  23,     1,  0,      timeHourHandler },
  { PARAM_TYPE_NUM2,    TXT_DELIM_COLON,    0,  59,     1,  0,      timeMinHandler  },
  { PARAM_TYPE_NUM2,    TXT_DELIM_NONE,     0,  59,     1,  0,      timeSecHandler  },
} DEFINE_DIALOG_END

DEFINE_DIALOG(dlgTimeZone) {
  //  ТИП               СТР                 МИН МАКС    ШАГ ЗНАК    ОБР
  { PARAM_TYPE_NUM,     TXT_DELIM_HOUR,     -12, 14,    1,  0,      timeZoneHourHandler },
  { PARAM_TYPE_NUM2,    TXT_DELIM_MINUTE,   -45, 45,    15, 0,      timeZoneMinHandler  },
  PARAM_EMPTY()
} DEFINE_DIALOG_END

DEFINE_DIALOG(dlgTimeSummer) {
  { PARAM_TYPE_STR,     strOffOnAuto,       0,  2,      1,  0,      timeSummerModeHandler   },
  PARAM_LABEL(TXT_DELIM_SPACE),
  { PARAM_TYPE_NUM,     TXT_DELIM_MINUTE,   0,  120,    15, 0,      timeSummerHandler       }
} DEFINE_DIALOG_END

DEFINE_DIALOG(dlgPresCurrent) {
  { PARAM_TYPE_NUM,     TXT_DELIM_SPACE,    0,  0,      0,  0,      pressureHandler         },
  { PARAM_TYPE_STR,     strPresUnit,        0,  1,      1,  0,      pressUnitHandler        },
  PARAM_EMPTY()
} DEFINE_DIALOG_END

DEFINE_DIALOG(dlgPresAltitude) {
  //  ТИП               СТР                 МИН МАКС    ШАГ ЗНАК    ОБР
  { PARAM_TYPE_NUM,     TXT_DELIM_SPACE,    0,  0,      0,  1,      pressAltHandler },
  PARAM_LABEL(TXT_DELIM_METER),
  PARAM_EMPTY()
} DEFINE_DIALOG_END

DEFINE_DIALOG(dlgPresDelta) {
  { PARAM_TYPE_NUM,     TXT_DELIM_SPACE,    0,  0,      0,  0,      pressDeltaHandler       },
  { PARAM_TYPE_STR,     strPresDeltaUnit,   0,  2,      1,  0,      pressDeltaUnitHandler   },
  PARAM_EMPTY()
} DEFINE_DIALOG_END

DEFINE_DIALOG(dlgPresValue) {
  { PARAM_TYPE_STR,     strPresValue,       0,  2,      1,  0,      pressValueHandler },
  PARAM_EMPTY(),
  PARAM_EMPTY()
} DEFINE_DIALOG_END


DEFINE_DIALOG(dlgLightCurrent) {
  //  ТИП               СТР                 МИН МАКС    ШАГ ЗНАК    ОБР
  { PARAM_TYPE_NUM,     TXT_DELIM_SPACE,    0,  0,      0,  1,      lightHandler },
  PARAM_LABEL(TXT_DELIM_LUX),
              PARAM_EMPTY()
} DEFINE_DIALOG_END

DEFINE_DIALOG(dlgLightAttenn) {
  { PARAM_TYPE_NUM,     TXT_DELIM_NONE,     0,  100,    1,  0,      lightCoeffHandler },
  PARAM_LABEL(TXT_DELIM_PRCNT),
  PARAM_EMPTY()
} DEFINE_DIALOG_END

DEFINE_DIALOG(dlgBrightness) {
  { PARAM_TYPE_NUM,     TXT_DELIM_NONE,     0,  16,     1,  0,      settBrightnessHandler },
  PARAM_EMPTY(),
  PARAM_EMPTY()
} DEFINE_DIALOG_END

DEFINE_DIALOG(dlgInversion) {
  //  ТИП               СТР                 МИН МАКС    ШАГ ЗНАК    ОБР
  { PARAM_TYPE_STR,     strOffOnAuto,       0,  2,      1,  0,      settInversionHandler },
  PARAM_EMPTY(),
  PARAM_EMPTY()
} DEFINE_DIALOG_END

DEFINE_DIALOG(dlgAutoOff) {
  { PARAM_TYPE_NUM,     TXT_DELIM_SPACE,    0,  120,    10, 0,      settAutoOffHandler },
  PARAM_LABEL(TXT_DELIM_SECOND),
  PARAM_EMPTY()
} DEFINE_DIALOG_END

DEFINE_DIALOG(dlgDisplayOff) {
  { PARAM_TYPE_NUM,     TXT_DELIM_SPACE,    0,  120,    10, 0,      settDisplayOffHandler },
  PARAM_LABEL(TXT_DELIM_SECOND),
  PARAM_EMPTY()
} DEFINE_DIALOG_END

DEFINE_DIALOG(dlgAltitude) {
  //  ТИП               СТР                 МИН МАКС    ШАГ ЗНАК    ОБР
  { PARAM_TYPE_NUM,     TXT_DELIM_NONE,     0,  99,     1,  0,      settAltitudeHiHandler },
  { PARAM_TYPE_NUM2,    TXT_DELIM_SPACE,    0,  99,     1,  0,      settAltitudeLoHandler },
  PARAM_LABEL(TXT_DELIM_METER)
} DEFINE_DIALOG_END

DEFINE_DIALOG(dlgLogPeriod) {
  { PARAM_TYPE_NUM,     TXT_DELIM_MINSP,    0,  120,    1,  0,      settLogTimeMinHandler },
  { PARAM_TYPE_NUM2,    TXT_DELIM_SECOND,   0,  55,     5,  0,      settLogTimeSecHandler },
  PARAM_EMPTY()
} DEFINE_DIALOG_END

DEFINE_DIALOG(dlgLogErase) {
  { PARAM_TYPE_NUM,     TXT_DELIM_NONE,     0,  1,      1,  0,      settLogEraseHandler },
  PARAM_EMPTY(),
  PARAM_EMPTY()
} DEFINE_DIALOG_END

DEFINE_DIALOG(dlgUpdate) {
  { PARAM_TYPE_STR,     strOffOnAuto,       0,  1,      1,  0,      settUpdateHandler },
  PARAM_EMPTY(),
  PARAM_EMPTY()
} DEFINE_DIALOG_END

DEFINE_DIALOG(dlgInfo) {
  PARAM_TEXT(TXT_INFO_AUTHOR),
  PARAM_TEXT(TXT_INFO_DATE),
  PARAM_TEXT(TXT_INFO_VERSION)
} DEFINE_DIALOG_END


/*******************************************************************************/
/*                                 МЕНЮ ПУНКТОВ                                */
/*******************************************************************************/

// меню ТЕМПЕРАТУРА
const settingItem_t siTemp[] PROGMEM = {
  { TXT_ITEM_CURRENT,   &dlgTempCurrent     },
  { TXT_ITEM_TFEEL,     &dlgTempFeel        },
  { TXT_ITEM_TDEWP,     &dlgTempDew         },
  { TXT_ITEM_HOME,      &dlgTempValue       }
};

// меню БАТАРЕЯ
const settingItem_t siBatt[] PROGMEM = {
  { TXT_ITEM_BATCH,     &dlgBattery         },
  { TXT_ITEM_BATLF,     &dlgBatteryLife     }
};

// меню ВЛАЖНОСТЬ
const settingItem_t siHum[] PROGMEM = {
  { TXT_ITEM_HREL,      &dlgHumidity        },
  { TXT_ITEM_HABS,      &dlgHumidityAbs     },
  { TXT_ITEM_HDEWP,     &dlgTempDew         },
  { TXT_ITEM_HSTEAM,    &dlgSteamPres       },
  { TXT_ITEM_HOME,      &dlgHumValue        }
};

// меню ДАТА И ВРЕМЯ
const settingItem_t siDate[] PROGMEM = {
  { TXT_ITEM_DTDATE,    &dlgDate            },
  { TXT_ITEM_DTTIME,    &dlgTime            },
  { TXT_ITEM_DTZONE,    &dlgTimeZone        },
  { TXT_ITEM_DTSUMT,    &dlgTimeSummer      }
};

// меню ДАВЛЕНИЕ
const settingItem_t siPres[] PROGMEM = {
  { TXT_ITEM_CURRENT, &dlgPresCurrent       },
  { TXT_ITEM_PALT,      &dlgPresAltitude    },
  { TXT_ITEM_PDELTA,    &dlgPresDelta       },
  { TXT_ITEM_HOME,      &dlgPresValue       }
};

// меню НАСТРОЙКИ
const settingItem_t siSett[] PROGMEM = {
  { TXT_ITEM_SBRIGH,    &dlgBrightness      },
  { TXT_ITEM_SINV,      &dlgInversion       },
  { TXT_ITEM_SAUTOF,    &dlgAutoOff         },
  { TXT_ITEM_SDISPF,    &dlgDisplayOff      },
  { TXT_ITEM_SALT,      &dlgAltitude        },
  { TXT_ITEM_SLOGPR,    &dlgLogPeriod       },
  { TXT_ITEM_SLOGER,    &dlgLogErase        },
  { TXT_ITEM_SUPDAT,    &dlgUpdate          },
  { TXT_ITEM_SABOUT,    &dlgInfo            }
};

// меню ОСВЕЩЕННОСТЬ
const settingItem_t siLight[] PROGMEM = {
  { TXT_ITEM_CURRENT,   &dlgLightCurrent    },
  { TXT_ITEM_LATTN,     &dlgLightAttenn     }
};


/*******************************************************************************/
/*                              МЕНЮ ДОМАШНЕГО ЭКРАНА                          */
/*******************************************************************************/

#define DEFINE_MENU_ITEM(settingItem, name) { sizeof(settingItem) / sizeof(settingItem_t), settingItem, name }

const MenuData_t menuHome[] PROGMEM = {
  DEFINE_MENU_ITEM(siTemp,  TXT_HDR_TEMP    ),
  DEFINE_MENU_ITEM(siBatt,  TXT_HDR_BATT    ),
  DEFINE_MENU_ITEM(siHum,   TXT_HDR_HUM     ),
  DEFINE_MENU_ITEM(siDate,  TXT_HDR_TIME    ),
  DEFINE_MENU_ITEM(siPres,  TXT_HDR_PRES    ),
  DEFINE_MENU_ITEM(siSett,  TXT_HDR_SETT    ),
  DEFINE_MENU_ITEM(siLight, TXT_HDR_LIGHT   )
};

#endif /* PAR_LIST_H */
