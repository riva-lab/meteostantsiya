#include "devices.h"
#include "SimpleTimeout.h"
#include "settings.h"
#include "handlers.h"
#include "log.h"
#include "sett_item.h"
#include "includes.h"
#include <GyverPower.h>

/*******************************************************************************/

SimpleSSD1306       disp;
BH1750FVI           myLux(LUXMETER_ADDRESS);
SimpleBME280        bme280;
SimpleSPIFlash      flash(PIN_FLASH_CS);
CustomButtons       btn;
SimpleMeteoCalc     mc;

SimpleTimeout       timeoutGetVCC(TIME_BATTERY);
SimpleTimeout       timeoutSecond(1000);
SimpleTimeout       timeoutMeasuring(TIME_MEASURING);

embedDateTime       utc;
t_datetime_struct   xt;
uint8_t             z, st;
float               luxValue;
uint8_t             sleepModeStatus = 0;

/*******************************************************************************/

// общая инициализация
void commonInit() {
  disp.begin(SSD1306_I2C_ADDRESS, 70);
  disp.repaint();
  disp.setFont(fontVerdana10Special);

  utc.rtc.begin();
  utc.begin();
  utc.setSummerTimeDelta(setting.timeDSTdelta);
  utc.setSummerTime(setting.timeDSTmode == SUMMERTIME_MODE_ON);
  utc.setSummerTimeAuto(setting.timeDSTmode == SUMMERTIME_MODE_AUTO);
  utc.setZone(setting.timeZone);

  bme280.begin();

  myLux.powerOn();
  myLux.setContHighRes();
  myLux.setCorrectionFactor(1.0);

  btn.begin();
  btn.addButton(PIN_BUTTON_UP, BUTTON_UP);
  btn.addButton(PIN_BUTTON_DOWN, BUTTON_DOWN);
  btn.addButton(PIN_BUTTON_LEFT, BUTTON_LEFT);
  btn.addButton(PIN_BUTTON_RIGHT, BUTTON_RIGHT);
  btn.addButton(PIN_BUTTON_OK, BUTTON_OK);

  Serial.begin(UART_BAUDRATE);

  // блокировка RESET МК, чтобы можно было подключать к хосту без выключения
  // чтобы залить новое ПО, нужно разблокировать RESET
  if (!setting.updateFlag) {
    _digitalWrite(PIN_LOCK_RESET, 1);
    _pinMode(PIN_LOCK_RESET, OUTPUT); // блокировка RESET МК
  }
}

// опрос датчиков и запись в лог
void measureWeatherParameters() {
  if (timeoutMeasuring.expired()) {
    timeoutMeasuring.restart();

    bme280.update();
    mc.setTemperature(bme280.getT());
    mc.setHumidity(bme280.getH());
    mc.setPressure(bme280.getP());
    mc.setUserAltitude(setting.deviceAltitude);

    mc.calculate();
  }

  if (myLux.isReady())
    luxValue = myLux.getLux();

  logWrite();
}


void powerState(uint8_t enable) {
  if (enable) {
    _pinMode(PIN_HOST_ACTIVE, INPUT);
    _pinMode(PIN_CHARGING_STATE, INPUT_PULLUP);
    _pinMode(PIN_LOCK_POWER, OUTPUT);
    _digitalWrite(PIN_LOCK_POWER, 1);
  } else {
    settingsSave();
    disp.power(0);
    disp.clear();
    disp.repaint();
    _digitalWrite(PIN_LOCK_POWER, 0);
    while (1) waitInSleep();
  }
}

uint8_t powerIsCharging() {
  return _digitalRead(PIN_CHARGING_STATE) == 0;
}

void powerOffTimersReset(uint8_t reset) {
  static uint8_t t1, t2;

  if (reset) {
    t1 = setting.timePowerOff;
    if (t2 = setting.timeDisplayOff) {
      sleepModeStatus = 0;
      disp.power(1);
    }
  }

  if (timeoutSecond.expired()) {
    timeoutSecond.restart();

    if (setting.timePowerOff) {
      if (t1) t1--;
      else    powerState(0);
    }

    if (setting.timeDisplayOff) {
      if (t2) {
        t2--;
      } else {
        sleepModeStatus = 1;
        disp.power(0);
      }
    }

    settInversionHandler(0, 0);
  }

  settBrightnessHandler(0, 0);
}

uint8_t isSleepMode() {
  return sleepModeStatus;
}

void waitInSleep() {
  if (_digitalRead(PIN_HOST_ACTIVE) == 0)
    delay(100);
  else {
    power.hardwareDisable(PWR_ALL);
    power.sleepDelay(100);
    power.hardwareEnable(PWR_ALL);
  }
}


uint16_t getSystemVoltage() {
  static uint16_t v;
  if (timeoutGetVCC.expired()) {
    timeoutGetVCC.restart();

    //reads internal 1V1 reference against VCC
    ADMUX = _BV(REFS0) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);  // For ATmega328
    uint16_t adc = 0;
    for (uint8_t i = 0; i < 32; i++) {
      delayMicroseconds(200);           // Wait for Vref to settle
      ADCSRA |= _BV(ADSC);              // Start conversion
      while (bit_is_set(ADCSRA, ADSC)); // measuring
      uint8_t low  = ADCL;              // must read ADCL first - it then locks ADCH
      uint8_t high = ADCH;              // unlocks both
      adc += (high << 8) | low;
    }
    v = 1023UL * 32 * ADC_UREF_CALIBRATED / adc; // расчёт реального VCC
  }
  // фильтр
  //  static uint16_t f = 3700;
  //  f = (float)f * (15.0 / 16.0) + (1023UL * 32 / 16 * vcc_const / adc);
  return v + SWITCH_DROPDOWN; // возвращает VCC в милливольтах
}

uint8_t liIonBatteryPercent() {
  uint16_t volts = getSystemVoltage();
  uint16_t capacity;
  if (volts > LIION_VOLTAGE_80) capacity = map(volts, LIION_VOLTAGE_100, LIION_VOLTAGE_80, 100, 80);
  else if ((volts <= LIION_VOLTAGE_80) && (volts > LIION_VOLTAGE_60) ) capacity = map(volts, LIION_VOLTAGE_80, LIION_VOLTAGE_60, 80, 60);
  else if ((volts <= LIION_VOLTAGE_60) && (volts > LIION_VOLTAGE_40) ) capacity = map(volts, LIION_VOLTAGE_60, LIION_VOLTAGE_40, 60, 40);
  else if ((volts <= LIION_VOLTAGE_40) && (volts > LIION_VOLTAGE_20) ) capacity = map(volts, LIION_VOLTAGE_40, LIION_VOLTAGE_20, 40, 20);
  else if (volts <= LIION_VOLTAGE_20) capacity = map(volts, LIION_VOLTAGE_20, LIION_VOLTAGE_0, 20, 0);
  capacity = constrain(capacity, 0, 100);
  return capacity;
}


void sendValue(uint32_t value) {
  Serial.write((uint8_t)(value & 0xFF)); value >>= 8;
  Serial.write((uint8_t)(value & 0xFF)); value >>= 8;
  Serial.write((uint8_t)(value & 0xFF)); value >>= 8;
  Serial.write((uint8_t)(value & 0xFF));
}

// связь с хостом по UART
uint8_t connectHostUpdate() {
  uint8_t status = 0;
  uint8_t *bytes;

  while (Serial.available()) {
    delay(1);

    if (Serial.read() != 'M') return 0; // запрос хоста
    Serial.write('R');                  // ответ МС

    char c = Serial.read();             // команда хоста
    Serial.write(c);                    // ответ МС

    if (c == 'L') {                     // команда запроса лога МС
      uint32_t address = 0;
      for (uint8_t i = 0; i < 4; i++) {
        address <<= 8;
        address |= (uint8_t)Serial.read();
      }

      flash.wakeUp();
      flash.setAddress(address * sizeof(MeteoLogRecord_t));
      for (uint8_t i = 0; i < sizeof(MeteoLogRecord_t); i++)
        Serial.write(flash.read());
      flash.sleep();
    }

    if (c == 'C') {                     // команда запроса текущих данных датчиков МС
      sendValue((uint32_t)mc.getHumidity());
      sendValue((uint32_t)mc.getPressure());
      sendValue((uint32_t)(mc.getTemperature() * 100.0));
      sendValue((uint32_t)luxValue);
    }

    if (c == 'I') {                     // команда запроса информации о МС
      Serial.print(getStringPGM(TXT_INFO_NAME));
      Serial.write(0);
      Serial.print(getStringPGM(TXT_INFO_VERSION));
      Serial.write(0);
      Serial.print(getStringPGM(TXT_INFO_DATE));
      Serial.write(0);
      Serial.print(getStringPGM(TXT_INFO_AUTHOR));
      Serial.write(0);
    }

    if (c == 'S') {                     // команда запроса настроек МС
      bytes = (uint8_t *)(&setting);
      for (uint8_t i = 0; i < sizeof(setting); i++)
        Serial.write(bytes[i]);
    }

    if (c == 'T') {                     // команда запроса текущего системного времени МС
      utc.update();
      uint32_t t = utc.getDateTime();
      bytes = (uint8_t *)(&t);
      for (uint8_t i = 0; i < sizeof(t); i++)
        Serial.write(bytes[i]);
    }

    if (c == 'E') {                     // команда очистки лога МС
      logErase();
      Serial.write('+');
    }

    status = 1;
  }

  return status;
}
