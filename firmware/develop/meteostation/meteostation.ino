#include "devices.h"
#include "settings.h"
#include "includes.h"
#include "gui.h"

/*******************************************************************************/

void setup() {
  powerState(1);            // вкл. питание
  settingsRestore();        // восстановить настройки
  powerOffTimersReset(1);   // сброс таймеров автовыключения
  commonInit();             // общая инициализация
}


void loop() {
  screenHome();             // отобразить домашний экран-меню
  powerState(0);            // выкл. питание
}
