﻿# Метеостанция

Проект автономной комнатной метеостанции на основе платы Adruino Nano V3 (Atmega328p) и датчика BME280.





## Ключевые особенности

- измерение температуры, влажности, давления — датчик **BME280**,
- измерение освещенности — датчик **BH1750FVI**,
- высокоточные часы реального времени **DS3231**,
- запись результатов периодических измерений в журнал — более 500 тыс. записей,
- отображение на дисплее **OLED 0,96"** SSD1306 128x64,
- удобное управление 5-кнопочной клавиатурой в виде джойстика,
- питание от аккумулятора **Li-Po 350 мА · ч**.







## Спецификация





### Метеопараметры

| Параметр     |            Диапазон             |          Точность          | Варианты отображения                                         | Варианты единиц измерения |
| ------------ | :-----------------------------: | :------------------------: | ------------------------------------------------------------ | :-----------------------: |
| Температура  |    0…65°C (рабочий –40…85°C)    |      ±1.0°C (0…65°C)       | измеренная, по ощущению, точка росы                          |         °C, °F, К         |
| Влажность    |     0…100% (относительная)      |        ±3% (20…80%)        | относительная, абсолютная, точка росы, давление насыщенного водяного пара |          %, г/м³          |
| Давление     | 30…110 кПа (225…825 мм рт. ст.) | ±0,1 кПа (0,75 мм рт. ст.) | давление, высота, разность                                   |     Па, мм рт. ст., м     |
| Освещенность |      1…65535 лк (прим. 1)       |            1 лк            |                                                              |            лк             |

> Примечание 1. Есть коррекция светопропускания окошка датчика освещенности.





### Электрические параметры

| Параметр                           | Значение | Единица измерения |
| ---------------------------------- | :------: | :---------------: |
| Питание                            | 3,3…4,2  |         В         |
| Емкость аккумулятора Li-Po         |   350    |      мА · ч       |
| Ток, в активном режиме, черный фон |    18    |        мА         |
| Ток, в режиме сна                  |   2,7    |        мА         |
| Автономная работа в ждущем режиме  |    ~5    |       суток       |








## Управление

Управление осуществляется при помощи 5 кнопок, расположенных в виде джойстика: `ВВЕРХ`, `ВНИЗ`, `ВЛЕВО`, `ВПРАВО`, `ОК`.

Включение — длинное нажатие `ОК`. Выключение — длинное нажатие `ОК` в режиме домашнего экрана.

Навигация по меню домашнего экрана — кнопки `ВЛЕВО` и `ВПРАВО`. Вход в подменю — `ОК`. Выход из подменю, возврат назад — длинное нажатие `ОК`. Изменение текущего редактируемого параметра (выделяется подчеркиванием) — кнопки `ВВЕРХ` и `ВНИЗ`.







## Схема

Полная схема проекта в [pdf](schematic/work/Метеостанция.pdf).

![](schematic/work/Метеостанция.png)







## Программное обеспечение

Исходный [код](firmware/develop/meteostation) проекта для Arduino Nano. В каталоге [libs](firmware/develop/meteostation/libs) находятся необходимые библиотеки. Как установить библиотеки для Arduino — читайте [здесь](https://doc.arduino.ua/ru/guide/Libraries). Проект построен на быстром и легком ядре [GyverCore for ATmega328](https://github.com/AlexGyver/GyverCore).

Скомпилированная [прошивка](firmware/release) для Arduino Nano (Atmega328p).







## Прототип

Метеостанция собрана в корпусе Z23A.

Больше [фото](docs/media/photo): [1](docs/media/photo/0001.jpg), [2](docs/media/photo/0002.jpg), [3](docs/media/photo/0004.jpg), [4](docs/media/photo/0005.jpg).

|         В корпусе Z23A         |        Собранная схема         |
| :----------------------------: | :----------------------------: |
| ![](docs/media/photo/0006.png) | ![](docs/media/photo/0003.jpg) |







## Авторство

Copyright 2021 Riva, open source hardware & software.

При распространении указывайте ссылку на этот [первоисточник](.).

